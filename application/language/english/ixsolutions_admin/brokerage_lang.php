<?php

/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['brokerage_action']              = 'Action';
$lang['brokerage_active']              = 'Active';
$lang['brokerage_invite_broker']       = 'Invite Broker';
$lang['brokerage_company']             = 'Broker Company';
$lang['brokerage_create_company']      = 'Create Company';
$lang['brokerage_created_on']          = 'Created on';
$lang['brokerage_deactivate_question'] = 'Are you sure you want to deactivate the product %s';
$lang['brokerage_deactivate_company'] = 'Are you sure you want to deactivate the Company %s';
$lang['brokerage_edit_broker']         = 'Edit broker';
$lang['brokerage_email']               = 'Email Address';
$lang['brokerage_name']                = 'Product name';
$lang['product_description']           = 'Product description';
$lang['brokerage_groups']              = 'Groups';
$lang['brokerage_inactive']            = 'Inactive';
$lang['brokerage_ip_address']          = 'IP address';
$lang['brokerage_last_login']          = 'Last login';
$lang['brokerage_lastname']            = 'Last name';
$lang['brokerage_member_of_groups']    = 'Member of groups';
$lang['brokerage_password']            = 'Password';
$lang['brokerage_password_confirm']    = 'Password confirm';
$lang['brokerage_phone']               = 'Phone';
$lang['brokerage_status']              = 'Status';
$lang['brokerage_brokerageName']       = 'User name / Pseudo';
$lang['brokerage_image']               = 'Brokerage Image';
$lang['brokerage_UsersName']           = 'User Name';
$lang['brokerage_companyName']           = 'Company Name';
$lang['brokerage_FirstName']           = 'First Name';
$lang['brokerage_LastName']           = 'Last Name';
$lang['brokerage_email']           = 'Email';
$lang['actions_submit']           = 'Send';
$lang['brokerage_status']           = 'Status';
$lang['brokerage_submit']           = 'Submit';



