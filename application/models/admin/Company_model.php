<?php

class Company_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /*
     * get all active product
     */
      
    public function get_product() {
        return $this->db2->get_where('product', 'status=1')->result();
    }

    /*
     * get all sic codes
     */
    

    public function get_sic_code() {
        return $this->db2->get('sicbox')->result();
    }

    /*
     * get  group
     */
    

    public function get_group() {
        $this->db2->where('name', 'company');
        return $this->db2->get('groups')->result();
    }

    /*
     * insert new user
     */
   

    public function insert_user($data) {
        $this->db2->insert('users', $data);
        $insert_id = $this->db2->insert_id();
        $query = $this->db2->last_query();
        $queryName = "INSERTED USER";
        $this->ion_auth->logFile($queryName, $query);
        return $insert_id;
    }

    /*
     * insert new users group
     */
     

    public function insert_user_type($data) {
        $this->db2->insert('users_groups', $data);
        $query = $this->db2->last_query();
        $queryName = "INSERTED USERTYPE";
        $this->ion_auth->logFile($queryName, $query);
    }

    /*
     * insert new company
     */
    

    public function insert_company($data) {
        $this->db2->insert('company', $data);
        $comp_id = $this->db2->insert_id();
        $query = $this->db2->last_query();
        $queryName = "INSERTED COMPANY";
        $this->ion_auth->logFile($queryName, $query);
        return $comp_id;
    }

    /*
     * insert company product
     */
    

    public function insert_company_product($data) {
        $this->db2->insert('company_product', $data);
        $query = $this->db2->last_query();
        $queryName = "INSERTED PRODUCT";
        $this->ion_auth->logFile($queryName, $query);
    }

    /*
     * get product wise question
     */
    

    public function get_question($pid) {

        $this->db2->where_in('prod_id', $pid);
        return $this->db2->get('questionnaires_questions')->result();
    }

    /*
     * get all answers
     */
    

    public function get_answer() {
        return $this->db2->get('questions_answers')->result();
    }

    /*
     * insert company wise question answer
     */
    

    public function insert_question_answer($data) {
        $this->db2->insert('company_ques_answer', $data);
        $query = $this->db2->last_query();
        $queryName = "INSERTED QUESTION_ANSWER";
        $this->ion_auth->logFile($queryName, $query);
    }

    /*
     * insert company's employee
     */
    

    public function insert_employee($data) {
        $this->db2->insert('employee', $data);
        $insert_id = $this->db2->insert_id();
        $query = $this->db2->last_query();
        $queryName = "INSERTED EMPLOYEE";
        $this->ion_auth->logFile($queryName, $query);
        return $insert_id;
    }

    /*
     * insert employee's relation data 
     */
    

    public function insert_employee_relation_data($data) {
        $this->db2->insert('employeespouse', $data);
        $query = $this->db2->last_query();
        $queryName = "INSERTED RELATION OF EMPLOYEE";
        $this->ion_auth->logFile($queryName, $query);
    }

    /*
     * get company's produt
     */
    

    public function get_comp_product($cid) {
        $this->db2->where('comp_id', $cid);
        return $this->db2->get('company_product')->result();
    }

    /*
     * for upload files
     */

    public function uploadFile($Files, $DestPath, $comp_name_id, $extra = '') {
        $FileName = '';
        $Lastpos = strrpos($Files['name'], '.');
        $FileExtension = strtolower(substr($Files['name'], $Lastpos, strlen($Files['name'])));
        $ValidExtensionArr = array(".jpg", ".jpeg", ".png", ".gif", ".pdf", ".doc", ".csv");
        if (in_array(strtolower($FileExtension), $ValidExtensionArr)) {
            if (!empty($extra)) {
                $FileName = time() . "_" . $extra . $FileExtension;
            } else {
                $FileName = $comp_name_id . $FileExtension;
            }
            
            if (move_uploaded_file($Files['tmp_name'], $DestPath . $FileName)) {
                $query = 'File IS BEING UPLOADED';
                $queryName = "UPLOADED FILE";
                $fileName = $FileName;
                $this->ion_auth->logFile($queryName, $query, $fileName);
                return $FileName;
            
            } else
                return false;
        }else {
            return false;
        }
    }

    /*
     * get perticular company
     */
    

    public function get_company($cid) {
        $this->db2->select('u.company');
        $this->db2->where('c.companyId', $cid);
        $this->db2->join('company c', 'c.userId=u.id');
        $query = $this->db2->get('users as u')->result();
        return $query[0]->company;
    }

    /*
     * get document type
     */
    

    public function get_document() {
        return $this->db2->get('document')->result();
    }

    /*
     * insert company document
     */
    

    public function insert_question_document($data) {
        $this->db2->insert('comp_emp_doc', $data);
        $query = $this->db2->last_query();
        $queryName = "INSERT QUESTION_DOCUMENTS";
        $this->ion_auth->logFile($queryName, $query);
    }

    /*
     * get company's no of employee 
     */
    

    public function get_comp_emp($cid) {
        $this->db2->select('no_of_emp');
        $this->db2->where('companyId', $cid);
        $query = $this->db2->get('company')->result();
        return $query[0]->no_of_emp;
    }

    /*
     * update no of employee of perticular company
     */
    public function update_company_employee($data, $cid) {
        $this->db2->where('companyId', $cid);
        $this->db2->update('company', $data);
        $query = $this->db2->last_query();
        $queryName = "UPDATE COMPANYEMPLOYEE";
        $this->ion_auth->logFile($queryName, $query);
    }

    /*
     * get company detail of perticular user 
     */
    public function get_company_detail($cid){
        $this->db2->select('*');
      if(!empty($cid))
         $this->db2->where('c.companyId',$cid);
        $this->db2->join('company c', 'c.userId=u.id');
        return $query = $this->db2->get('users as u')->result();
    }

    /*
     * get employee of perticular company
     */
    public function get_company_employee($cid) {
        $this->db2->where('companyId', $cid);
        return $this->db2->get('employee')->result();
    }

    /*
     * get employee relation data of perticular employee
     */
    

    public function get_company_employee_relation($employeeId) {
        $this->db2->where('employeeId', $employeeId);
        return $this->db2->get('employeespouse')->result();
    }
    

    public function get_company_all_employee($cid) {
        $this->db2->select('spouse.employeeId, spouse.employeeSpouseId, spouse.esFirstName, spouse.esLastName, spouse.gender as gen, spouse.dateOfBirth as birthdate, spouse.emp_relation');
        $this->db2->where('emp.companyId', $cid);
        $this->db2->join('employeespouse as spouse', 'emp.employeeId=spouse.employeeId');
        return $this->db2->get('employee as emp')->result();
    }
    

    public function update_employee($employee, $empid) {
        $this->db2->where('employeeId', $empid);
        $this->db2->update('employee', $employee);
         $query = $this->db2->last_query();
        $queryName = "UPDATE EMPLOYEE";
        $this->ion_auth->logFile($queryName, $query);
    }
    public function update_employee_relation_data($data, $sempid) {
        $this->db2->where('employeeSpouseId', $sempid);
        $this->db2->update('employeespouse', $data);
         $query = $this->db2->last_query();
        $queryName = "UPDATE EMPLOYEERELATION";
        $this->ion_auth->logFile($queryName, $query);
    }
   

    public function delete_employee_relation_data($sempid) {
        $this->db2->where('employeeSpouseId', $sempid);
        $this->db2->delete('employeespouse');
         $query = $this->db2->last_query();
        $queryName = "DELETED EMPLOYEERELATION";
        $this->ion_auth->logFile($queryName, $query);
    }
    

    public function delete_employee($empid) {
        $this->db2->where('employeeId', $empid);
        $this->db2->delete('employeespouse');
         $query = $this->db2->last_query();
        $queryName = "DELTED EMPLOYEESPOUSE";
        $this->ion_auth->logFile($queryName, $query);
        $this->db2->where('employeeId', $empid);
        $this->db2->delete('employee');
        $query = $this->db2->last_query();
        $queryName = "DELETED EMPLOYEE";
        $this->ion_auth->logFile($queryName, $query);
        
    }

    public function delete_employee_child($empid, $relation) {
        $this->db2->where('employeeId', $empid);
        $this->db2->where('emp_relation', $relation);
        $this->db2->delete('employeespouse');
        $query = $this->db2->last_query();
        $queryName = "DELETED EMPLOYEECHILD";
        $this->ion_auth->logFile($queryName, $query);
    }
    

    public function company_product_count($cid) {
        $this->db2->select('count(prod_id) as count');
        $this->db2->where('comp_id', $cid);
        $query = $this->db2->get('company_product')->result();
        return $query[0]->count;
    }
    

    public function get_occup_salary($cid) {
        $this->db2->select('count(p.productId) as count');
        $this->db2->where('c.comp_id', $cid);
        $this->db2->where('p.occup_salary', '1');
        $this->db2->join('company_product as c', 'c.prod_id=p.productId');
        $query = $this->db2->get('product as p')->result();
        return $query[0]->count;
    }
    

    public function get_company_all_detail($cid) {
        $this->db2->select('u.id, u.company, u.address, u.zipcode, u.country, u.state, u.city, u.email, c.sicId, c.no_of_emp, c.companyId, c.brokerage_name, c.broker_fname, c.broker_lname, c.npn, c.phone, c.email as bemail');
        $this->db2->where('c.companyId', $cid);
        $this->db2->join('company as c', 'c.userId=u.id');
        return $this->db2->get('users as u')->result();
    }
    

    public function update_user($data, $uid) {
        $this->db2->where('id', $uid);
        $this->db2->update('users', $data);
        $query = $this->db2->last_query();
        $queryName = "UPDATE USER";
        $this->ion_auth->logFile($queryName, $query);
    }
    

    public function update_company($data, $cid) {
        $this->db2->where('companyId', $cid);
        $this->db2->update('company', $data);
        $query = $this->db2->last_query();
        $queryName = "UPDATE COMPANY";
        $this->ion_auth->logFile($queryName, $query);
    }

    /* public function getBrokerIdByUserId($userId){
      return $this->db2->select('brokerId')->where('userId',$userId)->get('broker')->row_array();
      }
      public function get_brokerage_id($userId){
      return $this->db2->select('id')->where('userId',$userId)->get('brokerage')->row_array();
      } */
}

?>
