<?php

class Login_model extends CI_Model {
    
    function __construct()
    {
        parent::__construct();
    }
    public function updateLogOutAttemptById($record,$loginId){
        $this->db->where('logInId', $loginId);
        $this->db->update('user_log', $record);
    }
    public function addLoginAttemptsById($record)
    {
        $this->db->insert("user_log", $record);
        return $this->db->insert_id();
    }
    public function checkUserMailPassword($email,$password){
        return $this->db->select("id,first_name,last_name")
		->where("email", $email)->where("password", $password)->get("users")->row_array();
    }
}