<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questionnaire_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_questions(){
        return $this->db->get("questionnaires_questions")->result_array();
    }

    public function get_question_product($id){
        $this->db->select('name');
        $this->db->where('productId', $id);
        return $this->db->get("product")->row_array();
    }

    public function get_questions_by_id($id){
        $this->db->where('id', $id);
        return $this->db->get("questionnaires_questions")->row_array();
    }

    public function get_questions_answer_by_question_id($id){
        $this->db->where('question_id', $id);
        return $this->db->get("questions_answers")->result_array();
    }

    public function questionnaire_update($id, $data){
        return $this->db->where('id', $id)->update("questionnaires_questions", $data);
    }

    public function answer_update($id, $questionId, $data){
        $count = $this->db->where('id', $id)->where('question_id', $questionId)->get("questions_answers")->num_rows();
        if($count == 1){
            $this->db->where('id', $id)->where('question_id', $questionId)->update("questions_answers", $data);
            return $count;
        } else {
            return $count;
        }
    }

    public function question_insert($data){
        $this->db->insert("questionnaires_questions", $data);
        return $this->db->insert_id();
    }

    public function answer_insert($data){
        return $this->db->insert("questions_answers", $data);
    }

    public function questionnaire_answer_delete($id){
        $this->db->where('question_id', $id)->delete("questions_answers");
        return $this->db->where('id', $id)->delete("questionnaires_questions");
    }

    public function questionnaire_activate($id){
        $status = array('status'=> '1');
        return $this->db->where('id', $id)->update("questionnaires_questions", $status);
    }

    public function questionnaire_deactivate($id){
        $status = array('status'=> '0');
        return $this->db->where('id', $id)->update("questionnaires_questions", $status);
    }
}
