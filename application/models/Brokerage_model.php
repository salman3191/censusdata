<?php

class Brokerage_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }
    public function checkUserByEmail($email){
        return $this->db->select("email,id")
                ->where("email",$email)
                ->get('users')->row_array();
    }
    public function getBrokersList($id){
        $this->db->select();
        $this->db->from('users');
        $this->db->join('brokerage', 'brokerage.userId=users.id');
        $this->db->join('broker','broker.');
                
    }
}