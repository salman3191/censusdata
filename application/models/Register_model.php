<?php

class Register_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function hash_password($password, $salt = false, $use_sha1_override = FALSE) {
        if (empty($password)) {
            return FALSE;
        }

        // bcrypt
        if ($use_sha1_override === FALSE && $this->hash_method == 'bcrypt') {
            return $this->bcrypt->hash($password);
        }


        if ($this->store_salt && $salt) {
            return sha1($password . $salt);
        } else {
            $salt = $this->salt();
            return $salt . substr(sha1($salt . $password), 0, -$this->salt_length);
        }
    }
    public function salt()
	{

		$raw_salt_len = 16;

 		$buffer = '';
        $buffer_valid = false;

        if (function_exists('mcrypt_create_iv') && !defined('PHALANGER')) {
            $buffer = mcrypt_create_iv($raw_salt_len, MCRYPT_DEV_URANDOM);
            if ($buffer) {
                $buffer_valid = true;
            }
        }

        if (!$buffer_valid && function_exists('openssl_random_pseudo_bytes')) {
            $buffer = openssl_random_pseudo_bytes($raw_salt_len);
            if ($buffer) {
                $buffer_valid = true;
            }
        }

        if (!$buffer_valid && @is_readable('/dev/urandom')) {
            $f = fopen('/dev/urandom', 'r');
            $read = strlen($buffer);
            while ($read < $raw_salt_len) {
                $buffer .= fread($f, $raw_salt_len - $read);
                $read = strlen($buffer);
            }
            fclose($f);
            if ($read >= $raw_salt_len) {
                $buffer_valid = true;
            }
        }

        if (!$buffer_valid || strlen($buffer) < $raw_salt_len) {
            $bl = strlen($buffer);
            for ($i = 0; $i < $raw_salt_len; $i++) {
                if ($i < $bl) {
                    $buffer[$i] = $buffer[$i] ^ chr(mt_rand(0, 255));
                } else {
                    $buffer .= chr(mt_rand(0, 255));
                }
            }
        }

        $salt = $buffer;

        // encode string with the Base64 variant used by crypt
        $base64_digits   = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
        $bcrypt64_digits = './ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $base64_string   = base64_encode($salt);
        $salt = strtr(rtrim($base64_string, '='), $base64_digits, $bcrypt64_digits);

	    $salt = substr($salt, 0, $this->salt_length);


		return $salt;

	}
    public function addUser($record) {
        $this->db->insert("users", $record);
        return $this->db->insert_id();
    }

    public function addUserId_Broker($record) {
        $this->db->insert("broker", $record);
        $this->db->insert_id();
//        $query = $this->db->last_query();
    }

    public function addUserType($recordType) {
        $this->db->insert("users_groups", $recordType);
        return $this->db->insert_id();
    }

    public function getUserById($userId) {
        
    }

    public function getGroupIdByUserId($userId) {
        $this->db->select('users_groups.group_id');
        $this->db->from('users');
        $this->db->join('users_groups', 'users_groups.id = users_groups.userId', 'right')->where('users_groups.userId=', $userId);
        $query = $this->db->get();
        return $query->result();
    }

    public function getUserTypeById($id) {
        return $this->db->select("id,name")
                        ->where("id", $id)
                        ->get('groups')->result_array();
    }

    public function getUserType() {
        return $this->db->select("id,name")->get('groups')->result_array();
    }

    public function getUserTypeNameById($userTypeId) {
        return $this->db->select("name")->where('id', $userTypeId)->get('groups')->row_array();
    }

    public function checkEmail($email) {
        return $this->db->select("email")->where('email', $email)->get('users')->row_array();
    }

}
