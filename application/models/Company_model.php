<?php

class Company_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /*
     * get all active product
     */

    public function get_product() {
        return $this->db->get_where('product', 'status=1')->result();
    }

    /*
     * get all sic codes
     */

    public function get_sic_code() {
        return $this->db->get('sicbox')->result();
    }

    /*
     * get  group
     */

    public function get_group() {
        $this->db->where('name', 'company');
        return $this->db->get('groups')->result();
    }

    /*
     * insert new user
     */

    public function insert_user($data) {
        $this->db->insert('users', $data);
        $insert_id = $this->db->insert_id();
        $query = $this->db->last_query();
        $queryName = "INSERTED USER";
        $this->ixsolution_ion_auth->logFile($queryName, $query);
        return $insert_id;
    }

    /*
     * insert new users group
     */

    public function insert_user_type($data) {
        $this->db->insert('users_groups', $data);
        $query = $this->db->last_query();
        $queryName = "INSERTED USERTYPE";
        $this->ixsolution_ion_auth->logFile($queryName, $query);
    }

    /*
     * insert new company
     */

    public function insert_company($data) {

        $this->db->insert('company', $data);
        $comp_id = $this->db->insert_id();
        $query = $this->db->last_query();
        $queryName = "INSERTED COMPANY";
        $this->ixsolution_ion_auth->logFile($queryName, $query);
        return $comp_id;
    }

    /*
     * insert company product
     */

    public function insert_company_product($data) {
        $this->db->insert('company_product', $data);
        $query = $this->db->last_query();
        $queryName = "INSERTED PRODUCT";
        $this->ixsolution_ion_auth->logFile($queryName, $query);
        $prod_id = $this->db->insert_id();
    }

    /*
     * get product wise question
     */

    public function get_question($pid) {

        $this->db->where_in('prod_id', $pid);
        return $this->db->get('questionnaires_questions')->result();
    }

    /*
     * get all answers
     */

    public function get_answer() {
        return $this->db->get('questions_answers')->result();
    }

    /*
     * insert company wise question answer
     */

    public function insert_question_answer($data) {
        $this->db->insert('company_ques_answer', $data);
        $query = $this->db->last_query();
        $queryName = "INSERTED QUESTION_ANSWER";
        $this->ixsolution_ion_auth->logFile($queryName, $query);
    }

    /*
     * insert company's employee
     */

    public function insert_employee($data) {
        $this->db->insert('employee', $data);
        $insert_id = $this->db->insert_id();
        $query = $this->db->last_query();
        $queryName = "INSERTED EMPLOYEE";
        $this->ixsolution_ion_auth->logFile($queryName, $query);
        return $insert_id;
    }

    /*
     * insert employee's relation data 
     */

    public function insert_employee_relation_data($data) {
        $this->db->insert('employeespouse', $data);
        $query = $this->db->last_query();
        $queryName = "INSERTED EMPLOYEE SPOUSE";
        $this->ixsolution_ion_auth->logFile($queryName, $query);
    }

    /*
     * get company's produt
     */

    public function get_comp_product($cid) {
        $this->db->where('comp_id', $cid);
        return $this->db->get('company_product')->result();
    }

    /*
     * for upload files
     */

    public function uploadFile($Files, $DestPath, $comp_name_id, $extra = '') {
        $FileName = '';
        $Lastpos = strrpos($Files['name'], '.');
        $FileExtension = strtolower(substr($Files['name'], $Lastpos, strlen($Files['name'])));
        $ValidExtensionArr = array(".jpg", ".jpeg", ".png", ".gif", ".pdf", ".doc", ".csv");
        if (in_array(strtolower($FileExtension), $ValidExtensionArr)) {
            if (!empty($extra)) {
                $FileName = time() . "_" . $extra . $FileExtension;
            } else {
                $FileName = $comp_name_id . $FileExtension;
            }
            if (move_uploaded_file($Files['tmp_name'], $DestPath . $FileName)) {
                $query = 'FILE IS BEING UPLOADED';
                $queryName = "UPLOADED FILE";
                $fileName = $FileName;
                $this->ixsolution_ion_auth->logFile($queryName, $query, $fileName);
                return $FileName;
            } else
                return false;
        }else {
            return false;
        }
    }

    /*
     * get perticular company
     */

    public function get_company($cid) {
        $this->db->select('u.company');
        $this->db->where('c.companyId', $cid);
        $this->db->join('company c', 'c.userId=u.id');
        $query = $this->db->get('users as u')->result();
        return $query[0]->company;
    }

    /*
     * get document type
     */

    public function get_document() {
        return $this->db->get('document')->result();
    }

    /*
     * insert company document
     */

    public function insert_question_document($data) {
        $this->db->insert('comp_emp_doc', $data);
        $query = $this->db->last_query();
        $queryName = "INSERTED QUESTION_DOCUMENTS";
        $this->ixsolution_ion_auth->logFile($queryName, $query);
    }

    /*
     * get company's no of employee 
     */

    public function get_comp_emp($cid) {
        $this->db->select('no_of_emp');
        $this->db->where('companyId', $cid);
        $query = $this->db->get('company')->result();
        return $query[0]->no_of_emp;
    }

    /*
     * update no of employee of perticular company
     */

    public function update_company_employee($data, $cid) {
        $this->db->where('companyId', $cid);
        $this->db->update('company', $data);
        $query = $this->db->last_query();
        $queryName = "UPDATED COMPANY_EMPLOYEE";
        $this->ixsolution_ion_auth->logFile($queryName, $query);
    }

    /*
     * get company detail of perticular user 
     */

    public function get_company_detail($id, $user) {
        $this->db->select('*');
        if ($user == 'broker')
            $this->db->where('c.brokerId', $id);
        else if ($user == 'brokerage')
            $this->db->where('c.brokrage_id', $id);
        else
            $this->db->where('u.id', $id);
        $this->db->join('company c', 'c.userId=u.id');
        return $query = $this->db->get('users as u')->result();
    }

    /*
     * get employee of perticular company
     */

    public function get_company_employee($cid) {
        $this->db->where('companyId', $cid);
        return $this->db->get('employee')->result();
    }

    /*
     * get employee relation data of perticular employee
     */

    public function get_company_employee_relation($employeeId) {
        $this->db->where('employeeId', $employeeId);
        return $this->db->get('employeespouse')->result();
    }

    public function get_company_all_employee($cid) {
        $this->db->select('spouse.employeeId, spouse.employeeSpouseId, spouse.esFirstName, spouse.esLastName, spouse.gender as gen, spouse.dateOfBirth as birthdate, spouse.emp_relation');
        $this->db->where('emp.companyId', $cid);
        $this->db->join('employeespouse as spouse', 'emp.employeeId=spouse.employeeId');
        return $this->db->get('employee as emp')->result();
    }

    public function update_employee($employee, $empid) {
        $this->db->where('employeeId', $empid);
        $this->db->update('employee', $employee);
        $query = $this->db->last_query();
        $queryName = "UPDATED EMPLOYEE";
        $this->ixsolution_ion_auth->logFile($queryName, $query);
    }

    public function update_employee_relation_data($data, $sempid) {
        $this->db->where('employeeSpouseId', $sempid);
        $this->db->update('employeespouse', $data);
        $query = $this->db->last_query();
        $queryName = "UPDATED RELATION_DATA";
        $this->ixsolution_ion_auth->logFile($queryName, $query);
    }

    public function delete_employee_relation_data($sempid) {
        $this->db->where('employeeSpouseId', $sempid);
        $this->db->delete('employeespouse');
        $query = $this->db->last_query();
        $queryName = "DELETE EMPLOYEE_RELATION";
        $this->ixsolution_ion_auth->logFile($queryName, $query);
    }

    public function delete_employee($empid) {
        $this->db->where('employeeId', $empid);
        $this->db->delete('employeespouse');
        $query = $this->db->last_query();
        $queryName = "DELETE EMPLOYEE_SPOUSE";
        $this->ixsolution_ion_auth->logFile($queryName, $query);
        $this->db->where('employeeId', $empid);
        $this->db->delete('employee');
        $query = $this->db->last_query();
        $queryName = "DELETE EMPLOYEE";
        $this->ixsolution_ion_auth->logFile($queryName, $query);
    }

    public function delete_employee_child($empid, $relation) {
        $this->db->where('employeeId', $empid);
        $this->db->where('emp_relation', $relation);
        $this->db->delete('employeespouse');
        $query = $this->db->last_query();
        $queryName = "DELETE EMPLOYEE_CHILD";
        $this->ixsolution_ion_auth->logFile($queryName, $query);
    }

    public function company_product_count($cid) {
        $this->db->select('count(prod_id) as count');
        $this->db->where('comp_id', $cid);
        $query = $this->db->get('company_product')->result();
        return $query[0]->count;
    }

    public function get_occup_salary($cid) {
        $this->db->select('count(p.productId) as count');
        $this->db->where('c.comp_id', $cid);
        $this->db->where('p.occup_salary', '1');
        $this->db->join('company_product as c', 'c.prod_id=p.productId');
        $query = $this->db->get('product as p')->result();
        return $query[0]->count;
    }

    public function get_company_all_detail($cid) {
        $this->db->select('u.id, u.company, u.address, u.zipcode, u.country, u.state, u.city, u.email, c.sicId, c.no_of_emp, c.companyId');
        $this->db->where('c.companyId', $cid);
        $this->db->join('company as c', 'c.userId=u.id');
        return $this->db->get('users as u')->result();
    }

    public function update_user($data, $uid) {
        $this->db->where('id', $uid);
        $this->db->update('users', $data);
        $query = $this->db->last_query();
        $queryName = "UPDATED USER";
        $this->ixsolution_ion_auth->logFile($queryName, $query);
    }

    public function update_company($data, $cid) {
        $this->db->where('companyId', $cid);
        $this->db->update('company', $data);
        $query = $this->db->last_query();
        $queryName = "UPDATED COMPANY";
        $this->ixsolution_ion_auth->logFile($queryName, $query);
    }

    public function getBrokerIdByUserId($userId) {
        return $this->db->select('brokerId')->where('userId', $userId)->get('broker')->row_array();
    }

    public function get_brokerage_id($userId) {
        return $this->db->select('id')->where('userId', $userId)->get('brokerage')->row_array();
    }

}

?>
