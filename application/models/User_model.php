<?php

class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    public function getUserDetailsById($userId){
      return $this->db->where("id",$userId)->get("users")->row_array();
    }
     public function getUserTypeIdByName($name){
        return $this->db->select('id')->where('name',$name)->get('groups')->row_array();
    }
    public function getBrokerIdByUserId($userId){
        echo $userId;
        $query=$this->db->select('id')->where('userId',$userId)->get('brokerage')->row_array();
        print_r($query);
        exit;
    }
    public function getBrokerById($userId){
        $this->db->select('*');
        $this->db->from('brokerage');
        $this->db->join('broker','broker.brokrage_id=brokerage.id');
        $this->db->join('users', 'users.id = broker.userId');
        $this->db->where('brokerage.userId', $userId);
        return $query = $this->db->get()->result_array(); 
    }
    public function getUserTypeNameById($userId){
        $this->db->select('groups.name');
        $this->db->from('users');
        $this->db->join('users_groups','users_groups.userId=users.id','left');
        $this->db->join('groups','groups.id=users_groups.group_id','left');
        $this->db->where('users.id',$userId);
        return $query = $this->db->get()->row_array(); 
    }
    public function getEmailById($userId){
        return $this->db->select("email")->where("id",$userId)->get("users")->row_array();
    }
    public function upDateUserDetails($records,$userId){
        $this->db->where('id', $userId);
        $this->db->update('users', $records);
    }
    public function uploadFile($Files, $DestPath, $userFolderName,$extra = '') {
        $FileName = '';
        $Lastpos = strrpos($Files['name'], '.');
        $FileExtension = strtolower(substr($Files['name'], $Lastpos, strlen($Files['name'])));
        $ValidExtensionArr = array(".jpg", ".jpeg", ".png", ".gif");
        if (in_array(strtolower($FileExtension), $ValidExtensionArr)) {
            if (!empty($extra)) {
                $FileName = time() . "_" . $extra . $FileExtension;
            } else {
                $FileName = $userFolderName . $FileExtension;
            }
            if (move_uploaded_file($Files['tmp_name'], $DestPath . $FileName))
                return $FileName;
            else
                return false;
        }else {
            return false;
        }
    }
    public function getLastInsertedId(){
        $this->db->order_by("id", "DESC");
        return $this->db->select("id")->get('users')->row_array();
    }
    public function addBrokerageUser($record){
        $this->db->insert("users", $record);
        return $this->db->insert_id();
    }
    public function addBrokerUser($record){
        $this->db->insert("users", $record);
        return $this->db->insert_id();
    }
    public function addToBrokerageUserId($recordBrokerage){
        $this->db->insert("brokerage",$recordBrokerage);
        return $this->db->insert_id();
    }
    public function addToBrokerUserId($recordBroker){
        $this->db->insert("broker",$recordBroker);
        return $this->db->insert_id();
    }
    public function addToUserGroupId($recordGroup){
        $this->db->insert("users_groups",$recordGroup);
        return $this->db->insert_id();
    }
}
