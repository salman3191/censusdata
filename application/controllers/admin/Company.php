<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends Admin_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('admin/Company_model');
    }

    /*
     * company view 
     */

    public function index() {

        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Title Page */
            $this->page_title->push('Company');
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* TEST */
            $this->data['url_exist'] = is_url_exist('http://www.domprojects.com');
            /* Data */
            $this->data['product'] = $this->Company_model->get_product();
            $this->data['sic_code'] = $this->Company_model->get_sic_code();
            $this->data['company_detail'] = '';
            /* Load Template */
            $this->template->admin_render('admin/Company_view', $this->data);
        }
    }
    /*
     * get country state city from zipcode
     */

    public function getCountryStateCity() {
        $count_city = '';
        $coun_State_city = array();
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . $_GET['zipcode'] . '&sensor=false';
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
            exit;
        } else {
            $country = '';
            $obj = json_decode($response);
            $value = $obj->results[0]->address_components[1]->types[0];
            if ($value == 'locality') {
                $city = (isset($obj->results[0]->address_components[1]->long_name) ? $obj->results[0]->address_components[1]->long_name : '');
            }
            $value = $obj->results[0]->address_components[2]->types[0];
            if ($value == 'administrative_area_level_2') {
                $country = (isset($obj->results[0]->address_components[1]->long_name) ? $obj->results[0]->address_components[2]->long_name : '');
            }
            if (empty($country)) {
                $state = (isset($obj->results[0]->address_components[1]->long_name) ? $obj->results[0]->address_components[2]->long_name : '');
            } else {
                $state = (isset($obj->results[0]->address_components[1]->long_name) ? $obj->results[0]->address_components[3]->long_name : '');
            }
            $coun_State_city = array("country" => $country, "state" => $state, "city" => $city);
            echo json_encode($coun_State_city);
            exit;
        }
    }

    /*
     * insert company
     */

    public function insert_company() {
        if ($this->input->post('add') == 'add') {
            /* Variables */
            $tables = $this->config->item('tables', 'auth');
            $product = $this->input->post('product');
            $group = $this->Company_model->get_group();
            $memberType = $group[0]->name;
            $activationCode = md5(rand(1, 10000000000) . "fhsf" . rand(1, 100000));
            $email = $this->input->post('email');
            $cname = $this->input->post('cname');
            $user = array('username' => $cname,
                'address' => $this->input->post('address'),
                'zipcode' => $this->input->post('zipcode'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country'),
                'company' => $cname,
                'email' => $email,
                'activation_code' => $activationCode
            );
            $password = '';
            $id = $this->ion_auth->register($cname, $memberType, $password, $activationCode, $email, $user, '');
            $userAccountLink = site_url("auth/ga_user_activate/") . $id . "/" . $activationCode;
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            $company = array('userId' => $id,
                'sicId' => $this->input->post('sic'),
            'no_of_emp'=>$this->input->post('no_of_emp'),
            'brokerage_name'=>$this->input->post('brokerage_name'),
            'broker_fname'=>$this->input->post('bfname'),
            'broker_lname'=>$this->input->post('blname'),
            'npn'=>$this->input->post('npn'),
            'phone'=>$this->input->post('phone'),
            'email'=>$this->input->post('bemail'),
            'renewal_date'=>date('Y-m-d',strtotime($this->input->post('renewal_date')))
            );
            $cid = $this->Company_model->insert_company($company);

            foreach ($product as $product) {
                $comp_product = array('comp_id' => $cid,
                    'prod_id' => $product);
                $this->Company_model->insert_company_product($comp_product);
            }
            redirect('admin/Company/employee_view/' . $cid);
        } else {
            $email = $this->input->post('email');
            $cname = $this->input->post('cname');
            $user = array('username' => $cname,
                'address' => $this->input->post('address'),
                'zipcode' => $this->input->post('zipcode'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country'),
                'company' => $cname,
                'email' => $email
            );
            $this->Company_model->update_user($user, $this->input->post('userid'));
            $company = array('sicId' => $this->input->post('sic'),
                         'no_of_emp'=>$this->input->post('no_of_emp'),
                         'brokerage_name'=>$this->input->post('brokerage_name'),
                         'broker_fname'=>$this->input->post('bfname'),
                         'broker_lname'=>$this->input->post('blname'),
                         'npn'=>$this->input->post('npn'),
                         'phone'=>$this->input->post('phone'),
                         'email'=>$this->input->post('bemail'),
            );
            $this->Company_model->update_company($company, $this->input->post('companyid'));
            redirect('admin/Company/edit_employee_view/' . $this->input->post('companyid'));
        }
    }

    /*
     * employees data view
     */

    public function employee_view() {

        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Title Page */
            $this->page_title->push('Employees');
            $this->data['pagetitle'] = $this->page_title->show();
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* TEST */
            $this->data['url_exist'] = is_url_exist('http://www.domprojects.com');
            $this->data['company'] = $this->uri->segment(4);
            $this->data['company_emp'] = $this->Company_model->get_comp_emp($this->uri->segment(4));
            $this->data['occup_salary'] = $this->Company_model->get_occup_salary($this->uri->segment(4));
            $this->template->admin_render('admin/employee_view', $this->data);
        }
    }
    /*
     * employees data upload view
     */


    public function employee_upload_view() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');

        } else {


            /* Title Page */
            $this->page_title->push('Employees');
            $this->data['pagetitle'] = $this->page_title->show();
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* TEST */
            $this->data['url_exist'] = is_url_exist('http://www.domprojects.com');

            $this->data['company'] = $this->input->post('cid');

            $this->data['company_emp'] = $this->input->post('comp_emp');
            $this->template->admin_render('admin/employee_upload_view', $this->data);
        }
    }

    /*
     * insert employees data or upload employees data
     */

    public function insert_employee() {
        $product_count = 0;
        $product_count = $this->Company_model->company_product_count($this->input->post('cid'));
        $file_name = $_FILES['file_name'];
        if ($file_name) {
            $folderpath = $this->config->item('folderPathGA');
            $comp_name = $this->Company_model->get_company($this->input->post('cid'));
            $comp_name_id = $comp_name . "_" . $this->input->post('cid');
            if (!file_exists($folderpath . "employee_csv")) {
                mkdir($folderpath . "employee_csv", 0777);
            }

            $userPath = $folderpath . "employee_csv/";
            $file = $this->Company_model->uploadFile($_FILES['file_name'], $userPath, $comp_name_id);
            $fp = fopen($folderpath . "employee_csv/" . $file, "r");
            $flag = true;
            $id = 0;
            $last_name = '';
            $status = 0;
            $comp_emp = 0;
            while (($data = fgetcsv($fp, 1000, ",", "'")) !== false) {
                if ($flag) {
                    if ($data[0] == 'First Name' && $data[1] == 'Last Name' && $data[2] == 'Date of birth' && $data[3] == 'Salary' && $data[4] == 'occupation' && $data[5] == 'Address' && $data[6] == 'Phone no' && $data[7] == 'Join Date' && $data[8] == 'emailid' && $data[9] == 'maritial status' && $data[10] == 'relation' && $data[11] == 'ssn' && $data[12] == 'sex' && $data[13] == 'status') {
                        $flag = false;
                        continue;
                    } else
                        redirect('admin/Company/company_list_view');
                }


                if (substr($data[10], 0, 1) == 'e' || substr($data[10], 0, 1) == 'E') {
                    if ($data[13] == 'Active' || $data[13] == 'active') {
                        $status = 1;
                    }
                    $employee = array('companyId' => $this->input->post('cid'),
                        'firstName' => $data[0],
                        'lastName' => $data[1],
                        'dateofBirth' => date('Y-m-d', strtotime($data[2])),
                        'salary' => $data[3],
                        'occupation' => $data[4],
                        'address' => $data[5],
                        'phoneNumber' => $data[6],
                        'join_date' => date('Y-m-d', strtotime($data[7])),
                        'employeeEmailId' => $data[8],
                        'maritalStatus' => $data[9],
                        'ssn' => $data[11],
                        'gender' => $data[12],
                        'status' => $status,
                        'createdAt' => date('Y-m-d')
                    );
                    $id = $this->Company_model->insert_employee($employee);
                    $last_name = $data[1];
                    $comp_emp = $comp_emp + 1;
                } else if ((substr($data[10], 0, 1) == 's' || substr($data[10], 0, 1) == 'S') && $last_name == $data[1]) {
                    $spouse = array('employeeId' => $id,
                        'esFirstName' => $data[0],
                        'esLastName' => $data[1],
                        'dateOfBirth' => date('Y-m-d', strtotime($data[2])),
                        'emp_relation' => $data[10],
                        'gender' => $data[12]
                    );
                    $this->Company_model->insert_employee_relation_data($spouse);
                } else if ((substr($data[10], 0, 1) == 'c' || substr($data[10], 0, 1) == 'C') && $last_name == $data[1]) {
                    $child = array('employeeId' => $id,
                        'esFirstName' => $data[0],
                        'esLastName' => $data[1],
                        'dateOfBirth' => date('Y-m-d', strtotime($data[2])),
                        'emp_relation' => $data[10],
                        'gender' => $data[12]
                    );
                    $this->Company_model->insert_employee_relation_data($child);
                }
            }
             $company_employee=array('no_of_emp'=>$comp_emp);
            $this->Company_model->update_company_employee($company_employee,$this->input->post('cid'));
            
            redirect('admin/Company/company_list_view');

        } else if ($this->input->post('add') == 'update') {
            $count = $this->input->post('count');
            $comp_emp = $this->input->post('comp_emp');
            for ($i = 1; $i <= $count; $i++) {
                if ($this->input->post('fname' . $i) != '') {
                    $empid = $this->input->post('empid' . $i);
                    $employee = array('companyId' => $this->input->post('cid'),
                        'firstName' => $this->input->post('fname' . $i),
                        'lastName' => $this->input->post('lname' . $i),
                        'address' => $this->input->post('address' . $i),
                        'phoneNumber' => $this->input->post('phone' . $i),
                        'employeeEmailId' => $this->input->post('email' . $i),
                        'dateofBirth' => date('Y-m-d', strtotime($this->input->post('dob' . $i))),
                        'salary' => $this->input->post('salary' . $i),
                        'occupation' => $this->input->post('occupation' . $i),
                        'maritalStatus' => $this->input->post('status' . $i),
                        'createdAt' => date('Y-m-d'),
                        'join_date' => date('Y-m-d', strtotime($this->input->post('join_date' . $i))),
                        'ssn' => $this->input->post('ssn' . $i),
                        'gender' => $this->input->post('gender' . $i),
                        'status' => '1'
                    );
                    if ($empid != '')
                        $this->Company_model->update_employee($employee, $empid);
                    else
                        $empid = $this->Company_model->insert_employee($employee);

                    if ($this->input->post('sfname' . $i) != '' && $this->input->post('slname' . $i) != '' && $this->input->post('sdob' . $i) != '') {
                        $sempid = $this->input->post('sempid' . $i);
                        if ($sempid != '') {
                            $spouse = array(
                                'esFirstName' => $this->input->post('sfname' . $i),
                                'esLastName' => $this->input->post('slname' . $i),
                                'gender' => $this->input->post('sgender' . $i),
                                'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('sdob' . $i))),
                                'emp_relation' => 'spouse'
                            );
                            $this->Company_model->update_employee_relation_data($spouse, $sempid);
                        } else {
                            $spouse = array('employeeId' => $empid,
                                'esFirstName' => $this->input->post('sfname' . $i),
                                'esLastName' => $this->input->post('slname' . $i),
                                'gender' => $this->input->post('sgender' . $i),
                                'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('sdob' . $i))),
                                'emp_relation' => 'spouse'
                            );
                            $this->Company_model->insert_employee_relation_data($spouse);
                        }
                    }
                    $child_count = $this->input->post('child' . $i);
                    if ($child_count > 0) {
                        for ($j = 1; $j <= $child_count; $j++) {
                            $cempid = $this->input->post('cempid' . $i . "_" . $j);
                            if ($cempid != '') {
                                $child = array(
                                    'esFirstName' => $this->input->post('cfname' . $i . "_" . $j),
                                    'esLastName' => $this->input->post('clname' . $i . "_" . $j),
                                    'gender' => $this->input->post('cgender' . $i . "_" . $j),
                                    'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('cdob' . $i . "_" . $j))),
                                    'emp_relation' => 'child'
                                );
                                $this->Company_model->update_employee_relation_data($child, $cempid);
                            } else {
                                $child = array('employeeId' => $empid,
                                    'esFirstName' => $this->input->post('cfname' . $i . "_" . $j),
                                    'esLastName' => $this->input->post('clname' . $i . "_" . $j),
                                    'gender' => $this->input->post('cgender' . $i . "_" . $j),
                                    'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('cdob' . $i . "_" . $j))),
                                    'emp_relation' => 'child'
                                );
                                $this->Company_model->insert_employee_relation_data($child);
                            }
                        }
                    }
                }
            }
            $company_employee=array('no_of_emp'=>$comp_emp);
            $this->Company_model->update_company_employee($company_employee,$this->input->post('cid'));
            
            redirect('admin/Company/company_list_view');
        } else {
            $count = $this->input->post('count');
            $comp_emp = $this->input->post('comp_emp');
            for ($i = 1; $i <= $count; $i++) {
                if ($this->input->post('fname' . $i) != '') {
                    $employee = array('companyId' => $this->input->post('cid'),
                        'firstName' => $this->input->post('fname' . $i),
                        'lastName' => $this->input->post('lname' . $i),
                        'address' => $this->input->post('address' . $i),
                        'phoneNumber' => $this->input->post('phone' . $i),
                        'employeeEmailId' => $this->input->post('email' . $i),
                        'dateofBirth' => date('Y-m-d', strtotime($this->input->post('dob' . $i))),
                        'salary' => $this->input->post('salary' . $i),
                        'occupation' => $this->input->post('occupation' . $i),
                        'maritalStatus' => $this->input->post('status' . $i),
                        'createdAt' => date('Y-m-d'),
                        'join_date' => date('Y-m-d', strtotime($this->input->post('join_date' . $i))),
                        'ssn' => $this->input->post('ssn' . $i),
                        'gender' => $this->input->post('gender' . $i),
                        'status' => '1'
                    );
                    $id = $this->Company_model->insert_employee($employee);
                    if ($this->input->post('sfname' . $i) != '' && $this->input->post('slname' . $i) != '' && $this->input->post('sdob' . $i) != '') {
                        $spouse = array('employeeId' => $id,
                            'esFirstName' => $this->input->post('sfname' . $i),
                            'esLastName' => $this->input->post('slname' . $i),
                            'gender' => $this->input->post('sgender' . $i),
                            'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('sdob' . $i))),
                            'emp_relation' => 'spouse'
                        );
                        $this->Company_model->insert_employee_relation_data($spouse);
                    }
                    $child_count = $this->input->post('child' . $i);
                    if ($child_count > 0) {
                        for ($j = 1; $j <= $child_count; $j++) {
                            $child = array('employeeId' => $id,
                                'esFirstName' => $this->input->post('cfname' . $i . "_" . $j),
                                'esLastName' => $this->input->post('clname' . $i . "_" . $j),
                                'gender' => $this->input->post('cgender' . $i . "_" . $j),
                                'dateOfBirth' => date('Y-m-d', strtotime($this->input->post('cdob' . $i . "_" . $j))),
                                'emp_relation' => 'child'
                            );
                            $this->Company_model->insert_employee_relation_data($child);
                        }
                    }
                }
            }
            $company_employee = array('no_of_emp' => $comp_emp);
            $this->Company_model->update_company_employee($company_employee, $this->input->post('cid'));

            if ($comp_emp < 10 || $product_count == 0) {
                redirect('admin/Company/company_list_view');
            } else {
                redirect('admin/Company/question_answer_view/' . $this->input->post('cid'));
            }
        }
    }

    /*
     * questionnaries view
     */

    public function question_answer_view() {

        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Title Page */
            $this->page_title->push('Questionnaries');
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* TEST */
            $this->data['url_exist'] = is_url_exist('http://www.domprojects.com');
            $cid = $this->uri->segment(4);

            $product = $this->Company_model->get_comp_product($cid);
            $pid[] = '';
            foreach ($product as $pro) {
                $pid[] = $pro->prod_id;
            }
            $this->data['question'] = $this->Company_model->get_question($pid);
            $this->data['answer'] = $this->Company_model->get_answer();
            $this->data['cid'] = $cid;
            /* Load Template */
            $this->template->admin_render('admin/question_answer_view', $this->data);
        }
    }
    /*
     * questionnaries upload view
     */

    public function question_answer_upload_view() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Title Page */
            $this->page_title->push('Questionnaries');
            $this->data['pagetitle'] = $this->page_title->show();
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* TEST */
            $this->data['url_exist'] = is_url_exist('http://www.domprojects.com');
            $cid = $this->input->post('cid');
            $this->data['document'] = $this->Company_model->get_document();
            $this->data['cid'] = $cid;
            $this->template->admin_render('admin/question_answer_upload_view', $this->data);
        }
    }

    /*
     * insert question or upload questions
     */

    public function insert_question_answer() {
        $folderpath = $this->config->item('folderPathGA');

        $cid = $this->input->post('cid');
        $file_name = $_FILES['file_name'];
        
        if ($file_name) {
            $comp_name = $this->Company_model->get_company($cid);
            $comp_name_id = $comp_name . "_" . $cid;
            if (!file_exists($folderpath . "document")) {
                mkdir($folderpath . "document", 0777);
            }
            $userPath = $folderpath . "document/";
            $file = $this->Company_model->uploadFile($_FILES['file_name'], $userPath, $comp_name_id);
            $question_document = array('comp_id' => $cid,
                'doc_id' => $this->input->post('file_type'),
                'document_name' => $file
            );
            $this->Company_model->insert_question_document($question_document);
        } else {
            $cnt = $this->input->post('cnt');
            for ($i = 1; $i < $cnt; $i++) {
                $ans_count = count($this->input->post('aid' . $i . '[]'));
                for ($j = 0; $j < $ans_count; $j++) {
                    $question_answer = array("questionId" => $this->input->post('qid' . $i),
                        "ques_answer" => $this->input->post('ques_answer' . $i),
                        "answerId" => $this->input->post('aid' . $i . '[' . $j . ']'),
                        "companyId" => $cid
                    );
                    $this->Company_model->insert_question_answer($question_answer);
                }
            }
        }
        //redirect('Company/employee_view/'.$cid);
        redirect('admin/Company/company_list_view');
    }

    /*
     * display all company which inserted by user
     */

    function company_list_view() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {

            /* Title Page */
            $this->page_title->push('Company List');
            $this->data['pagetitle'] = $this->page_title->show();
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* TEST */
            $this->data['url_exist'] = is_url_exist('http://www.domprojects.com');
            $this->data['company_detail']=$this->Company_model->get_company_detail('');
            $this->data['member'] = $this->ion_auth->is_member($this->data['user_login']['id']);
            $this->template->admin_render('admin/company_list_view', $this->data);
        }
    }

    /*
     * csv for export all employee's data of perticular company
     */

    public function export_company_detail() {
        $cid = $this->uri->segment(4);
        $comp_name = $this->Company_model->get_company($cid);
        $comp_name_id = $comp_name . "_" . $cid;
        $this->load->helper('download');
        $detail = $this->Company_model->get_company_employee($cid);
        if (!file_exists("download")) {
            mkdir("download", 0777);
        }
        $fp = fopen("download/" . $comp_name_id . ".csv", "w");
        $head = array("Employee Id", "First Name", "Last Name", "Relation", "SSN", "Birth Date", "Sex", "Address", "Email", "Hire Date", "Status");
        fputcsv($fp, $head);
        $write_info = array();
        foreach ($detail as $detail) {
            $write_info['employeeid'] = $detail->employeeId;
            $write_info['first_name'] = $detail->firstName;
            $write_info['last_name'] = $detail->lastName;
            $write_info['relation'] = "Employee";
            $write_info['ssn'] = $detail->ssn;
            $write_info['birth_date'] = date('d/m/Y', strtotime($detail->dateOfBirth));
            $write_info['gender'] = $detail->gender;
            $write_info['address'] = $detail->address;
            $write_info['email'] = $detail->employeeEmailId;
            $write_info['join_date'] = date('d/m/Y', strtotime($detail->join_date));
            if ($detail->status == 1) {
                $write_info['status'] = "Active";
            } else {
                $write_info['status'] = "Inactive";
            }
            fputcsv($fp, $write_info);
            $relation_detail = $this->Company_model->get_company_employee_relation($detail->employeeId);
            if (!empty($relation_detail)) {
                foreach ($relation_detail as $rel_detail) {
                    $write_info['employeeid'] = $rel_detail->employeeId;
                    $write_info['first_name'] = $rel_detail->esFirstName;
                    $write_info['last_name'] = $rel_detail->esLastName;
                    $write_info['relation'] = $rel_detail->emp_relation;
                    $write_info['ssn'] = $rel_detail->ssn;
                    $write_info['birth_date'] = date('d/m/Y', strtotime($rel_detail->dateOfBirth));
                    $write_info['gender'] = $rel_detail->gender;
                    $write_info['address'] = "";
                    $write_info['email'] = "";
                    $write_info['join_date'] = "";
                    $write_info['status'] = "";
                    fputcsv($fp, $write_info);
                }
            }
        }
        fclose($fp);
        $file_name = "download/" . $comp_name_id . ".csv";
        $query = 'FILE IS BEING EXPORTED';
        $queryName = "EXPORTED FILE";
        $fileName = $file_name;
        $this->ion_auth->logFile($queryName, $query, $fileName);
        force_download($file_name, NULL);
    }

    public function edit_employee_view() {
        $cid = $this->uri->segment(4);
        $this->data['company'] = $cid;
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Title Page */
            $this->page_title->push('Edit Employee');
            $this->data['pagetitle'] = $this->page_title->show();
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* TEST */
            $this->data['url_exist'] = is_url_exist('http://www.domprojects.com');
            $this->data['employee_detail'] = $this->Company_model->get_company_employee($cid);
            $this->data['company_emp'] = $this->Company_model->get_comp_emp($cid);
            $this->data['employee_relation'] = $this->Company_model->get_company_all_employee($cid);
            $this->data['occup_salary'] = $this->Company_model->get_occup_salary($cid);
            $this->template->admin_render('admin/employee_view', $this->data);
        }
    }

    public function delete_employee($empid) {
        $this->Company_model->delete_employee($empid);
        return;
    }

    public function delete_employee_relation_data($id) {
        $this->Company_model->delete_employee_relation_data($id);
        return;
    }

    public function delete_employee_child($empid, $relation) {
        $this->Company_model->delete_employee_child($empid, $relation);
        return;
    }

    public function employee_upload_format() {
        $this->load->helper('download');
        $folderpath = $this->config->item('folderPathGA');
        $userPath = $folderpath . "employee_csv/";
        $file_name = $userPath . "employee_upload_example.csv";
        $query = null;
        $queryName = "UPLOADED FILE";
        $fileName = $file_name;
        $this->ion_auth->logFile($queryName, $query, $fileName);
        force_download($file_name, NULL);
    }
    public function update_company_view() {

        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            $cid = $this->uri->segment(4);

            /* Title Page */
            $this->page_title->push('Edit Company');
            $this->data['pagetitle'] = $this->page_title->show();
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* TEST */
            $this->data['url_exist'] = is_url_exist('http://www.domprojects.com');
            $this->data['company_detail'] = $this->Company_model->get_company_all_detail($cid);
            $this->data['product'] = '';
            $this->data['sic_code'] = $this->Company_model->get_sic_code();
            $this->data['company'] = $cid;
            $this->template->admin_render('admin/Company_view', $this->data);
        }
    }

}

?>