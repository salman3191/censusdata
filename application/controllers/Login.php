<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends Ixsolutions_admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model("login_model");
        $this->load->model("register_model");
        $this->load->library('session');
        $this->lang->load('auth');
    }

    public function index() {
        if (isset($_POST['submit'])) {
            $email = $_POST['txtEmail'];
            $password = md5($_POST['txtPassword']);
            $getInfo = $this->login_model->checkUserMailPassword($email, $password);
            if (empty($getInfo)) {
                $this->session->set_flashdata('message', 'Invalid User Email or Password');
                $this->data['message'] = $this->session->flashdata('message');
                $this->load->view('login/index', $this->data);
            }
            if ($getInfo > 0) {
                $userId = $getInfo['id'];
                $firstName = $getInfo['first_name'];
                $lastName = $getInfo['last_name'];
                $userType = $this->register_model->getUserTypeById($getInfo['userTypeId']);
                $userTypeId = $userType[0]['id'];
                $date = date('Y/m/d H:i:s');
                $record = array("user_id" => $getInfo['id'], "logInTime" => $date);
                $loginId = $this->login_model->addLoginAttemptsById($record);
                $data = array("userId" => $userId, "firstName" => $firstName, 'lastName' => $lastName);
                if ($countLog >= 0) {
                    $session_data = array(
                        "userId" => $brokerageId['id'], "firstName" => $firstName, 'lastName' => $lastName, "id" => $userId, "loginId" => $loginId);
                    $this->session->set_userdata('user', $session_data);
                    $id = $this->register_model->getGroupIdByUserId($userId);
                    if ($id['0']->group_id == 3) {
                        redirect(base_url() . "brokerage/index");
                    }
                    if ($id['0']->group_id == 4) {
                        redirect(base_url() . "broker/index");
                    } else {
                        redirect(base_url() . "user/index", $data);
                    }
                } else {
                    $session_data = array("userId" => 0);
                    $this->session->set_userdata('user', $session_data);
                }
            }
        } else {
            $data['message'] = '';
        }
        
        $title = "Login";
        $navbardiv = array(
            'subclass' => 'inverse',
            'top' => 'static',
            'role' => 'navigation',
            'fluid' => FALSE,
        );

        $data['viewport'] = TRUE;
        $data['body_role'] = 'document';
        $data['custom_css'] = 'carousel';
        $data['title'] = $title;
        $data['navbar'] = $this->navbar;
        $data['navbar_dropdown'] = $this->navbar_dropdown;
        $data['navbar_right'] = $this->navbar_right;
        $data['navbardiv'] = $navbardiv;
        $data['navfoot'] = $this->navfoot;
        $data['carousel'] = $this->carousel_page;
        $this->load->view('frontend/head', $data);
        $this->load->view('frontend/navbar/open_wrapper');
        $this->load->view('frontend/navbar/open_nav');
        $this->load->view('frontend/navbar/open_container');
        $this->load->view('frontend/navbar/header');
        $this->load->view('frontend/navbar/open_collapse');
        $this->load->view('frontend/navbar/open_bar');
        $this->load->view('frontend/navbar/li_core', $data);
        $this->load->view('frontend/navbar/right', $data);
        $this->load->view('frontend/navbar/close_bar');
        $this->load->view('frontend/navbar/close');
        $this->load->view('frontend/navbar/close_wrapper');
        $this->load->view('login/index', $data);
        $this->load->view('frontend/container/close');
        $this->load->view('frontend/footer', $data);
        $this->load->view('frontend/foot_docs', $data);
    }

    public function logout() {
        $id = $this->session->userdata['user']['loginId'];
        $date = date('Y/m/d H:i:s');
        $record = array("logOutTime" => $date);
        $this->login_model->updateLogOutAttemptById($record, $id);
        $this->session->userdata();
        $this->session->unset_userdata('user');
        redirect(base_url());
    }

}
