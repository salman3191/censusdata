<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ixsolutions_auth extends Ixsoultion_my_Controller  {

    function __construct() {
        parent::__construct();

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ixsolution_ion_auth'), $this->config->item('error_end_delimiter', 'ixsolution_ion_auth'));
        $this->load->helper('url');
        $this->lang->load('auth');
        $this->lang->load('ixsolution_ion_auth');
        $this->load->library('session');
        $this->load->model('ixsolution_admin/Ixsolutions_ion_auth_model');
        $loginId = '';
    }

    function index() {
        if (!$this->ixsolution_ion_auth->logged_in()) {
            redirect('ixsolutions_auth/login', 'refresh');
        } else {
            redirect('/', 'refresh');
        }
    }

    function login() {

        if (!$this->ixsolution_ion_auth->logged_in()) {
            /* Load */
            $this->load->config('ixsolutions_admin/dp_config');
            $this->load->config('ixsolutions_common/dp_config');

            /* Valid form */
            $this->form_validation->set_rules('identity', 'Identity', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            /* Data */
            $this->data['title']               = $this->config->item('title');
            $this->data['title_lg']            = $this->config->item('title_lg');
            $this->data['auth_social_network'] = $this->config->item('auth_social_network');
            $this->data['forgot_password']     = $this->config->item('forgot_password');
            $this->data['new_membership']      = $this->config->item('new_membership');

            if ($this->form_validation->run() == TRUE) {
                $remember = (bool) $this->input->post('remember');

                if ($this->ixsolution_ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
                    if (!$this->ixsolution_ion_auth->logged_in()) {
                        $this->session->set_flashdata('message', $this->ixsolution_ion_auth->messages());
                        redirect('ixsolutions_auth/login', 'refresh');
                    } else {
                        /* Data */
                        $date = date('Y/m/d H:i:s');
                        $userId = $this->ixsolution_ion_auth->get_user_id();
                        $record = array("user_id" => $userId, "logInTime" => $date);
                        $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                        $loginId = $this->ixsolutions_ion_auth_model->addLoginAttemptsById($record);
                        $sessionData = array ('logInId'=>$loginId);
                        $this->session->set_userdata('user', $sessionData);
                        /* Load Template */
                        redirect('ixsolutions_admin', 'refresh');
//                        $this->template->auth_render('auth/choice', $this->data);
                        
                    }
                } else {
                    $this->session->set_flashdata('message', $this->ixsolution_ion_auth->errors());
                    redirect('ixsolutions_auth/login', 'refresh');
                }
            } else {
                $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

                $this->data['identity'] = array(
                    'name'        => 'identity',
                    'id'          => 'identity',
                    'type'        => 'email',
                    'value'       => $this->form_validation->set_value('identity'),
                    'class'       => 'form-control',
                    'placeholder' => lang('auth_your_email')
                );
                $this->data['password'] = array(
                    'name'        => 'password',
                    'id'          => 'password',
                    'type'        => 'password',
                    'class'       => 'form-control',
                    'placeholder' => lang('auth_your_password')
                );

                /* Load Template */
                $this->template->auth_render('ixsolutions_auth/login', $this->data);
            }
        } else {
            redirect('/', 'refresh');
        }
    }

    function logout($src = NULL) {
        $logout = $this->ixsolution_ion_auth->logout();
        $id =$this->session->userdata['user']['logInId'];
        $date = date('Y/m/d H:i:s');
        $record = array("logOutTime" => $date);
        $this->ixsolutions_ion_auth_model->updateLogOutAttemptById($record,$id);
        $this->session->unset_userdata('user');
        $this->session->set_flashdata('message', $this->ixsolution_ion_auth->messages());

        if ($src == 'admin') {
            redirect('ixsolutions_auth/login', 'refresh');
        } else {
            redirect('/', 'refresh');
        }
    }

    public function ga_user_activate($id, $code = FALSE) {
        
        $id                      = (int) $id;
        $this->data['pagetitle'] = 'Complete your profile';
        $this->data['title']     = $this->config->item('title');
        $this->data['title_lg']  = $this->config->item('title_lg');
        /* Data */
        $user                    = $this->ixsolution_ion_auth->user($id)->row();
        $groups                  = $this->ixsolution_ion_auth->groups()->result_array();
        $currentGroups           = $this->ixsolution_ion_auth->get_users_groups($id)->result();
        /* Validate form input */
        $this->form_validation->set_rules('first_name', 'lang:edit_user_validation_fname_label', 'required');
        $this->form_validation->set_rules('last_name', 'lang:edit_user_validation_lname_label', 'required');
        $this->form_validation->set_rules('phone', 'lang:edit_user_validation_phone_label', 'required');
        
        if (isset($_POST) && !empty($_POST)) {
            
//            echo "<pre/>";print_r($_POST);exit();
            if ($id != $this->input->post('id')) {
                show_error($this->lang->line('error_csrf'));
            }

            if ($this->input->post('password')) {
                $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ixsolutions_ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ixsolutions_ion_auth') . ']|matches[password_confirm]');
                $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
            }

            if ($this->form_validation->run() == TRUE) {
                $data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name'  => $this->input->post('last_name'),
                    'username'   => $this->input->post('first_name') . " " . $this->input->post('last_name'),
                    'company'    => $this->input->post('company'),
                    'phone'      => $this->input->post('phone')
                );

                if ($this->input->post('password')) {
                    $data['password'] = $this->input->post('password');
                }

                if ($this->ixsolution_ion_auth->is_admin()) {
                    $groupData = $this->input->post('groups');
                    if (isset($groupData) && !empty($groupData)) {
                        $this->ixsolution_ion_auth->remove_from_group('', $id);

                        foreach ($groupData as $grp) {
                            $this->ixsolution_ion_auth->add_to_group($grp, $id);
                        }
                    }
                }
                if ($this->ixsolution_ion_auth->update($user->id, $data)) {
                    $activation = $this->ixsolution_ion_auth->activate($id, $code);
                    $this->session->set_flashdata('message', $this->ixsolution_ion_auth->messages());
                    $this->logout('admin');
                } else {
                    $this->session->set_flashdata('message', $this->ixsolution_ion_auth->errors());
                    if ($this->ixsolution_ion_auth->is_admin()) {
                        redirect('ixsolutions_auth/login', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }
                }
            }
        }
        
        // set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ixsolution_ion_auth->errors() ? $this->ixsolution_ion_auth->errors() : $this->session->flashdata('message')));
        // pass the user to the view
        $this->data['user']             = $user;
        $this->data['groups']           = $groups;
        $this->data['currentGroups']    = $currentGroups;
        $this->data['first_name']       = array(
            'name'        => 'first_name',
            'id'          => 'first_name',
            'type'        => 'text',
            'class'       => 'form-control',
            'placeholder' => 'First Name',
            'value'       => (!empty($this->form_validation->set_value('first_name', $user->first_name)) ? $this->form_validation->set_value('first_name', $user->first_name) : '')
        );
        $this->data['last_name']        = array(
            'name'        => 'last_name',
            'id'          => 'last_name',
            'type'        => 'text',
            'class'       => 'form-control',
            'placeholder' => 'Last Name',
            'value'       => (!empty($this->form_validation->set_value('last_name', $user->last_name)) ? $this->form_validation->set_value('last_name', $user->last_name) : '')
        );
        $this->data['phone']            = array(
            'name'        => 'phone',
            'id'          => 'phone',
            'type'        => 'tel',
            'pattern'     => '^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$',
            'class'       => 'form-control',
            'placeholder' => 'Contact Number',
            'value'       => ''
        );
        $this->data['password']         = array(
            'name'        => 'password',
            'id'          => 'password',
            'class'       => 'form-control',
            'placeholder' => 'Password',
            'type'        => 'password'
        );
        $this->data['password_confirm'] = array(
            'name'        => 'password_confirm',
            'id'          => 'password_confirm',
            'class'       => 'form-control',
            'placeholder' => 'Confirm Password',
            'type'        => 'password'
        );
        /* Load Template */
        $this->template->auth_render('ixsolutions_auth/edit', $this->data);
    }

}
