<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Brokerage extends Ixsolutions_admin_Controller {
    public function __construct() {
        parent::__construct();
        /* Load :: Common */
        
        $this->lang->load('ixsolutions_admin/brokerage');
        
        $this->load->model('ixsolution_admin/brokerage_model');
        /* Title Page :: Common */
        $this->page_title->push(lang('menu_brokerage'));
        $this->data['pagetitle'] = $this->page_title->show();
        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_brokers'), 'ixsolutions_admin/brokerage/index');
    }
    public function index() {
        if (!$this->ixsolution_ion_auth->logged_in()) {
            redirect('ixsolutions_auth/login', 'refresh');
        } else {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();
            /* Get all Brokers */
            $brokerageId = $this->ixsolution_ion_auth->get_user_id();
            $this->data['records'] = $this->brokerage_model->getBrokersById($brokerageId);
            /* Load Template */
//            echo "<pre/>";print_r($this->data['records'] );exit();
            $this->ixsolutions_template->admin_render('ixsolutions_admin/brokerage/index',$this->data);
        }
    }
    public function inviteBroker(){
        $registeredID = '';
        $this->breadcrumbs->unshift(2, lang('invite_broker'), 'ixsolutions_admin/brokerage/inviteBroker');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();
        $tables = $this->config->item('tables', 'ixsolutions_ion_auth');
        /* Validate form input */
        $this->form_validation->set_rules('first_name', 'lang:users_firstname', 'required');
        $this->form_validation->set_rules('last_name', 'lang:users_lastname', 'required');
        $this->form_validation->set_rules('email', 'lang:users_email', 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        if ($this->form_validation->run() == TRUE) {
            $username = strtolower($this->input->post('first_name')) . ' ' . strtolower($this->input->post('last_name'));
            $email = strtolower($this->input->post('email'));
            $memberType = 'broker';
//            $password = $this->input->post('password');
            $password = '';
            $activationCode = md5(rand(1, 10000000000) . "fhsf" . rand(1, 100000));
            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'company' => '',
                'phone' => '',
                'active' => 0
            );
            $record = array();
            $registeredID = $this->ixsolution_ion_auth->register($username, $memberType, $password, $activationCode, $email, $additional_data, $record);
        }
        if ($registeredID) {
            $userId = $this->ixsolution_ion_auth->get_user_id();
            $brokerageId = $this->brokerage_model->get_brokerage_id($userId);
            $broker = array('brokerId'=>'', 'userId'=>$registeredID,'brokrage_id'=>$brokerageId['id']);
            $this->brokerage_model->insertBroker($broker);
            $this->session->set_flashdata('message', $this->ixsolution_ion_auth->messages());
            redirect('ixsolutions_admin/brokerage', 'refresh');
        } else {
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ixsolution_ion_auth->errors() ? $this->ixsolution_ion_auth->errors() : $this->session->flashdata('message')));
            $this->data['first_name'] = array(
                'name' => 'first_name',
                'id' => 'first_name',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('first_name'),
            );
            $this->data['last_name'] = array(
                'name' => 'last_name',
                'id' => 'last_name',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('last_name'),
            );
            $this->data['email'] = array(
                'name' => 'email',
                'id' => 'email',
                'type' => 'email',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('email'),
            );
            $this->data['membertype'] = array(
                'name' => 'membertype',
                'id' => 'membertype',
                'type' => 'membertype',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('membertype'),
            );
            $this->load->model('ixsolution_admin/preferences_model');
            $result = $this->preferences_model->get_interface('groups');
            foreach ($result as $Index => $resultArray) {
                $optionArray[$resultArray['name']] = $resultArray['name'];
            }
            $this->data['options'] = $optionArray;
            /* Load Template */
            $this->ixsolutions_template->admin_render('ixsolutions_admin/brokerage/inviteBroker',$this->data);
        }
        
    }
    public function delete() {
        /* Load Template */
        $this->ixsolutions_template->admin_render('ixsolutions_admin/users/delete', $this->data);
    }
    public function activate($id, $code = FALSE) {
        $id = (int) $id;

        if ($code !== FALSE) {
            $activation = $this->ixsolution_ion_auth->activate($id, $code);
        } else if ($this->ixsolution_ion_auth->logged_in()) {
            $activation = $this->ixsolution_ion_auth->activate($id);
        }

        if ($activation) {
            $this->session->set_flashdata('message', $this->ixsolution_ion_auth->messages());
            redirect('ixsolutions_admin/brokerage', 'refresh');
        } else {
            $this->session->set_flashdata('message', $this->ixsolution_ion_auth->errors());
            redirect('auth/forgot_password', 'refresh');
        }
    }
    public function deactivate($id = NULL) {
        if (!$this->ixsolution_ion_auth->logged_in() OR ! $this->ixsolution_ion_auth->logged_in()) {
            return show_error('You must be an administrator to view this page.');
        }

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_users_deactivate'), 'ixsolutions_admin/brokerage/deactivate');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Validate form input */
        $this->form_validation->set_rules('confirm', 'lang:deactivate_validation_confirm_label', 'required');
        $this->form_validation->set_rules('id', 'lang:deactivate_validation_user_id_label', 'required|alpha_numeric');

        $id = (int) $id;

        if ($this->form_validation->run() === FALSE) {
            $user = $this->ixsolution_ion_auth->user($id)->row();

            $this->data['csrf'] = $this->_get_csrf_nonce();
            $this->data['id'] = (int) $user->id;
            $this->data['firstname'] = !empty($user->first_name) ? htmlspecialchars($user->first_name, ENT_QUOTES, 'UTF-8') : NULL;
            $this->data['lastname'] = !empty($user->last_name) ? ' ' . htmlspecialchars($user->last_name, ENT_QUOTES, 'UTF-8') : NULL;

            /* Load Template */
            $this->ixsolutions_template->admin_render('ixsolutions_admin/brokerage/deactivate', $this->data);
        } else {
            if ($this->input->post('confirm') == 'yes') {
                if ($this->_valid_csrf_nonce() === FALSE OR $id != $this->input->post('id')) {
                    show_error($this->lang->line('error_csrf'));
                }

                if ($this->ixsolution_ion_auth->logged_in() && $this->ixsolution_ion_auth->logged_in()) {
                    $this->ixsolution_ion_auth->deactivate($id);
                }
            }

            redirect('ixsolutions_admin/brokerage', 'refresh');
        }
    }
    public function activateCompany($id, $brokerId ,$code = FALSE) {
        $id = (int) $id;
        if ($code !== FALSE) {
            $activation = $this->ixsolution_ion_auth->activate($id, $code);
        } else if ($this->ixsolution_ion_auth->logged_in()) {
            $activation = $this->ixsolution_ion_auth->activate($id);
        }
        if ($activation) {
            $this->session->set_flashdata('message', $this->ixsolution_ion_auth->messages());
            redirect('ixsolutions_admin/brokerage/companyList/'.$brokerId, 'refresh');
        } else {
            $this->session->set_flashdata('message', $this->ixsolution_ion_auth->errors());
            redirect('auth/forgot_password', 'refresh');
        }
    }
    public function deactivateCompany($id = NULL, $brokerId,$code = FALSE) {
        if (!$this->ixsolution_ion_auth->logged_in() OR ! $this->ixsolution_ion_auth->logged_in()) {
            return show_error('You must be an administrator to view this page.');
        }
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_users_deactivate'), 'ixsolutions_admin/brokerage/deactivateCompany');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();
        /* Validate form input */
        $this->form_validation->set_rules('confirm', 'lang:deactivate_validation_confirm_label', 'required');
        $this->form_validation->set_rules('id', 'lang:deactivate_validation_user_id_label', 'required|alpha_numeric');
        $id = (int) $id;
        if ($this->form_validation->run() === FALSE) {
            $user = $this->ixsolution_ion_auth->user($id)->row();
            $this->data['csrf'] = $this->_get_csrf_nonce();
            $this->data['id'] = (int) $user->id;
            $this->data['brokerId'] = (int) $brokerId;
            $this->data['firstname'] = !empty($user->first_name) ? htmlspecialchars($user->first_name, ENT_QUOTES, 'UTF-8') : NULL;
            $this->data['lastname'] = !empty($user->last_name) ? ' ' . htmlspecialchars($user->last_name, ENT_QUOTES, 'UTF-8') : NULL;
            /* Load Template */
            $this->ixsolutions_template->admin_render('ixsolutions_admin/brokerage/deactivateCompany', $this->data);
        } else {
            if ($this->input->post('confirm') == 'yes') {
                if ($this->_valid_csrf_nonce() === FALSE OR $id != $this->input->post('id')) {
                    show_error($this->lang->line('error_csrf'));
                }
                if ($this->ixsolution_ion_auth->logged_in() && $this->ixsolution_ion_auth->logged_in()) {
                    $this->ixsolution_ion_auth->deactivate($id);
                }
            }
            redirect('ixsolutions_admin/brokerage/companyList/'.$brokerId, 'refresh');
        }
    }
    public function profile($id) {
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_brokerage_profile'), 'ixsolutions_admin/groups/profile');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();
        /* Data */
        $id = (int) $id;
        $this->data['user_info'] = $this->ixsolution_ion_auth->user($id)->result();
        foreach ($this->data['user_info'] as $k => $user) {
            $this->data['user_info'][$k]->groups = $this->ixsolution_ion_auth->get_users_groups($user->id)->result();
        }
        /* Load Template */
        $this->ixsolutions_template->admin_render('ixsolutions_admin/brokerage/profile', $this->data);
    }
    public function _get_csrf_nonce() {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }
    public function _valid_csrf_nonce() {
        if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE && $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function companyList($id = null){
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_company'), 'ixsolutions_admin/brokerage/index');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();
        $this->data['records'] = $this->brokerage_model->get_broker_companylist_byBrokerId($id);
        $this->ixsolutions_template->admin_render('ixsolutions_admin/brokerage/companyList',$this->data);
    }
}
