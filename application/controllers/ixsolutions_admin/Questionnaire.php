<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Questionnaire extends Ixsolutions_admin_Controller
{

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->lang->load('ixsolutions_admin/questionnaire');

        $this->load->helper(array('form', 'url'));
        /* Title Page :: Common */
        $this->page_title->push(lang('menu_questionnaire'));
        $this->data['pagetitle'] = $this->page_title->show();
        $this->load->model('ixsolution_admin/questionnaire_model');
        $this->load->model('ixsolution_admin/product_model');
        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_questionnaire'), 'ixsolutions_admin/questionnaire');
    }

    public function index()
    {
        if (!$this->ixsolution_ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        } else {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Get all products */
            $this->data['questions'] = $this->questionnaire_model->get_all_questions();
            foreach ($this->data['questions'] as $k => $question) {
                $this->data['questions'][$k]['product'] = $this->questionnaire_model->get_question_product($question['prod_id']);
            }
            /* Load Template */
            $this->ixsolutions_template->admin_render('ixsolutions_admin/questionnaire/index', $this->data);
        }
    }

    public function create()
    {
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_questionnaire_create'), 'ixsolutions_admin/questionnaire/create');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Variables */
        $tables = $this->config->item('tables', 'ion_auth');

        /* Validate form input */
        $this->form_validation->set_rules('products_name', 'lang:product_id', 'required');
        $this->form_validation->set_rules('questionnaire_question', 'lang:questionnaire_question', 'required');
        $this->form_validation->set_rules('questionnaire_selection', 'lang:questionnaire_selection', 'required');

        if ($this->form_validation->run() == TRUE) {
            $additional_data = array(
                'prod_id' => $this->input->post('products_name'),
                'question' => $this->input->post('questionnaire_question'),
                'question_desc' => $this->input->post('questionnaire_description'),
                'question_type' => $this->input->post('questionnaire_selection'),
            );
            $insertedID = $this->questionnaire_model->question_insert($additional_data);
        }
        if ($this->form_validation->run() == TRUE && $insertedID) {
            foreach ($_POST['questionnaire_answer'] as $answer) {
                $answerArray = array(
                    'question_id' => $insertedID,
                    'answer' => $answer
                );
                $this->questionnaire_model->answer_insert($answerArray);
            }
            $this->session->set_flashdata('message', $this->ixsolution_ion_auth->messages());
            redirect('ixsolutions_admin/questionnaire', 'refresh');
        } else {
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ixsolution_ion_auth->errors() ? $this->ixsolution_ion_auth->errors() : $this->session->flashdata('message')));
            $result = $this->product_model->get_all_products();
            foreach ($result as $Index => $resultArray) {
                $optionArray[$resultArray['productId']] = $resultArray['name'];
            }
            $this->data['options'] = $optionArray;

            $this->data['products_name'] = array(
                'name' => 'products_name',
                'id' => 'products_name',
                'type' => 'products_name',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('products_name'),
            );
            $this->data['questionnaire_question'] = array(
                'name' => 'questionnaire_question',
                'id' => 'questionnaire_question',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('questionnaire_question'),
            );
            $this->data['questionnaire_description'] = array(
                'name' => 'questionnaire_description',
                'id' => 'questionnaire_description',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('questionnaire_description'),
            );
            $this->data['questionnaire_selection'] = array(
                'name' => 'questionnaire_selection',
                'id' => 'questionnaire_selection',
                'type' => 'questionnaire_selection',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('questionnaire_question'),
            );
            $optionArraySelection = array(
                'single' => 'Single Select',
                'multiple' => 'Multiple Select'
            );
            $this->data['options_selection'] = $optionArraySelection;
            /* Load Template */
            $this->ixsolutions_template->admin_render('ixsolutions_admin/questionnaire/create', $this->data);
        }
    }

    public function delete($id)
    {
        $this->questionnaire_model->questionnaire_answer_delete($id);
        redirect('ixsolutions_admin/questionnaire', 'refresh');
        /* Load Template */
    }

    public function edit($id)
    {
        $id = (int)$id;
        if (!$this->ixsolution_ion_auth->logged_in()) {
            redirect('ixsolutions_auth', 'refresh');
        }

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_products_edit'), 'ixsolutions_admin/product/edit');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Data */
        $questions['question'] = $this->questionnaire_model->get_questions_by_id($id);
        $questions['answer'] = $this->questionnaire_model->get_questions_answer_by_question_id($id);
        /* Validate form input */
        $this->form_validation->set_rules('products_name', 'lang:edit_product_validation_pname_label', 'required');

        if (isset($_POST) && !empty($_POST)) {

            if ($this->form_validation->run() == TRUE) {
                $data = array(
                    'prod_id' => $this->input->post('products_name'),
                    'question' => $this->input->post('questionnaire_question'),
                    'question_desc' => $this->input->post('questionnaire_description'),
                    'question_type' => $this->input->post('questionnaire_selection'),
                );

                if ($this->questionnaire_model->questionnaire_update($this->input->post('id'), $data)) {

                    foreach ($_POST['questionnaire_answer'] as $index => $answer) {
                        $answerArray = array(
                            'question_id' => $this->input->post('id'),
                            'answer' => $answer
                        );
                        $answerResult = $this->questionnaire_model->answer_update($index, $this->input->post('id'), $answerArray);
                        if ($answerResult == '0')
                            $this->questionnaire_model->answer_insert($answerArray);
                    }

                    $this->session->set_flashdata('message', $this->ixsolution_ion_auth->messages());

                    if ($this->ixsolution_ion_auth->is_admin()) {
                        redirect('ixsolutions_admin/questionnaire', 'refresh');
                    } else {
                        redirect('admin', 'refresh');
                    }
                } else {
                    $this->session->set_flashdata('message', $this->ixsolution_ion_auth->errors());

                    if ($this->ixsolution_ion_auth->is_admin()) {
                        redirect('auth', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }
                }
            }
        }

        // display the edit user form
        $this->data['csrf'] = $this->_get_csrf_nonce();

        // set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ixsolution_ion_auth->errors() ? $this->ixsolution_ion_auth->errors() : $this->session->flashdata('message')));

        // pass the user to the view
        $this->data['question'] = $questions;
        $result = $this->product_model->get_all_products();
        foreach ($result as $Index => $resultArray) {
            $optionArray[$resultArray['productId']] = $resultArray['name'];
        }
        $this->data['options'] = $optionArray;
        $this->data['products_name'] = array(
            'name' => 'products_name',
            'id' => 'products_name',
            'type' => 'products_name',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('products_name', $questions['question']['prod_id']),
        );
        $this->data['questionnaire_question'] = array(
            'name' => 'questionnaire_question',
            'id' => 'questionnaire_question',
            'type' => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('questionnaire_question', $questions['question']['question']),
        );
        $this->data['questionnaire_description'] = array(
            'name' => 'questionnaire_description',
            'id' => 'questionnaire_description',
            'type' => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('questionnaire_description',$questions['question']['question_desc']),
        );
        $this->data['questionnaire_selection'] = array(
            'name' => 'questionnaire_selection',
            'id' => 'questionnaire_selection',
            'type' => 'questionnaire_selection',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('questionnaire_question'),
        );
        $optionArraySelection = array(
            'single' => 'Single Select',
            'multiple' => 'Multiple Select'
        );
        $this->data['options_selection'] = $optionArraySelection;

        /* Load Template */
        $this->ixsolutions_template->admin_render('ixsolutions_admin/questionnaire/edit', $this->data);
    }

    public function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    function activate($id)
    {
        $id = (int)$id;
        $activation = $this->questionnaire_model->questionnaire_activate($id);
        if ($activation) ;
        redirect('ixsolutions_admin/questionnaire', 'refresh');
    }

    public function deactivate($id = NULL)
    {
        $id = (int)$id;
        $deactivation = $this->questionnaire_model->questionnaire_deactivate($id);
        if ($deactivation) ;
        redirect('ixsolutions_admin/questionnaire', 'refresh');
    }

    public function profile($id)
    {
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_users_profile'), 'ixsolutions_admin/groups/profile');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Data */
        $id = (int)$id;

        $this->data['user_info'] = $this->ixsolution_ion_auth->user($id)->result();
        foreach ($this->data['user_info'] as $k => $user) {
            $this->data['user_info'][$k]->groups = $this->ixsolution_ion_auth->get_users_groups($user->id)->result();
        }

        /* Load Template */
        $this->ixsolutions_template->admin_render('ixsolutions_admin/users/profile', $this->data);
    }

    public function _valid_csrf_nonce()
    {
        if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE && $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
