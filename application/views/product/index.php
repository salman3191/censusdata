<?php if($product['productId'] == '') : ?>
    <div class="container" >
    <div class="row">
        <div class="col-xs-offset-2 col-md-9" style="margin-top:10%;">
            <label class="col-lg-12" style="text-align: center;">Add Product</label>
            <?php echo ($message) ? $message : ''; ?>
            <form class="form-horizontal" action ="addProduct" name="productAdd" method="POST" enctype="multipart/form-data">
                <div class="col-md-8">
                    <div class="form-group" >
                        <lable name="lblProductName" class="col-md-3 label-heading asterisk">Product Name </lable>
                        <div class='col-md-8'>
                            <input type="text"  name="txtProductName" id="productName" class="form-control" value="" placeholder="Product Name" required/>
                        </div>
                    </div>
                    <div class="form-group" >
                        <lable name="lblDescription" class="col-md-3 label-heading asterisk">Description</lable>
                        <div class='col-md-8'>
                            <textarea name="txtDescription" id="description" class="form-control" placeholder="Description" required/></textarea>
                        </div>
                    </div>
                    <div class="form-group" >
                        <lable name="lblProductImage" class="col-md-3">Product Image</lable>
                        <div class='col-md-8'>
                            <input type="file" name="FImage" id="productImage" class="form-control" placeholder="Choose Product Pic..." value="" required/>
                        </div>
                    </div>
                    <div class="form-group" >
                        <div class='col-md-9 col-md-offset-4' >
                            <input type="button" class="btn btn-danger btn-cancel-action cancelBrokerage" value="Cancel" id="cancelBrokerage" style="margin-right: 10px;" onclick="location.href = '<?php echo site_url() . "product/viewProducts"; ?>';">
                            <input type="submit"  name="submit"  class="btn btn-success btn-login-submit" value="Save" />
                        </div>
                    </div>
                </div>
        </div>
        </form>
    </div>
</div>
</div>
<?php else :  ?>
    <div class="container" >
    <div class="row">
        <div class="col-xs-offset-2 col-md-9" style="margin-top:10%;">
                <label class="col-lg-12" style="text-align: center;">Edit Product</label>
           <?php echo ($message) ? $message : ''; ?>
            <form class="form-horizontal" action ="#" name="productAdd" method="POST" enctype="multipart/form-data">
                <div class="col-md-8">
                    <div class="form-group" >
                        <lable name="lblProductName" class="col-md-3 label-heading asterisk">Product Name </lable>
                        <div class='col-md-8'>
                            <input type="text"  name="txtProductName" id="productName" class="form-control" value="<?php echo $product['name']; ?>" placeholder="Product Name" required/>
                        </div>
                    </div>
                    <div class="form-group" >
                        <lable name="lblDescription" class="col-md-3 label-heading asterisk">Description</lable>
                        <div class='col-md-8'>
                            <textarea name="txtDescription" id="description" class="form-control" placeholder="Description" required/><?php echo $product['description']; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group" >
                        <lable name="lblProductImage" class="col-md-3">Product Image</lable>
                        <div class='col-md-8'>
                            <input type="file" name="FImage" id="productImage" class="form-control" placeholder="Choose Product Pic..." value="<?php echo ($product['productImage'])?$product['productImage']: ''; ?>" />
                        </div>
                    </div>
                    <?php if ($product['name']): ?> 
                        <div class="form-group" >
                            <lable name="lblStatus" class="col-md-3">Status</lable>
                            <div class='col-md-8'>
                                <input type="radio" name="status" id="status" value="1" <?php echo ($product['status']==1)? "checked" : '' ; ?> required/> Enable 
                                <input type="radio" name="status" id="status" value="0" <?php echo ($product['status']==0)? "checked" : '' ; ?> required/> Disable 
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="form-group" >
                        <div class='col-md-9 col-md-offset-4' >
                            <input type="hidden" name="productId" value="<?php echo $product['productId']; ?>">
                            <input type="hidden" name="imageName" value="<?php echo $product['productImage']; ?>">
                            <input type="button" class="btn btn-danger btn-cancel-action cancelBrokerage" value="Cancel" id="cancelBrokerage" style="margin-right: 10px;" onclick="location.href = '<?php echo site_url() . "product/viewProduct"; ?>';">
                            <input type="submit"  name="update"  class="btn btn-success btn-login-submit" value="Save" />
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <?php if ($product['name']): ?>   
                        <img src = "<?php echo base_url() . "upload/Product/" . $product['name'] . "/" . $product['productImage']; ?>" height="250px" width="250px" class="img-circle" alt="<?php echo $product['name']; ?>">
                    <?php endif; ?>
                </div>
            </form>
        </div>
    </div>
</div>
<?php endif;?>