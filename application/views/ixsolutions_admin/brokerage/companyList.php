<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo anchor('company', '<i class="fa fa-plus"></i> ' . lang('brokerage_create_company'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th><?php echo lang('brokerage_UsersName'); ?></th>
                                    <th><?php echo lang('brokerage_companyName'); ?></th>
                                    <th><?php echo lang('brokerage_email'); ?></th>
                                    <th><?php echo lang('brokerage_status'); ?></th>
                                    <th><?php echo lang('brokerage_action'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($records as $user): ?>
                                    <tr>
                                        <td>
                                            <?php echo ($user['username']) ? $user['username'] : '--'; ?>
                                        </td>
                                        <td><?php echo ($user['company']) ? $user['company'] : '--'; ?></td>
                                        <td><?php echo $user['email']; ?></td>
                                        <td><?php echo ($user['active']) ? anchor('ixsolutions_admin/brokerage/deactivateCompany/' . $user['id']. "/".$user['brokerId'], '<span class="label label-success">' . lang('brokerage_active') . '</span>') : anchor('ixsolutions_admin/brokerage/activateCompany/' . $user['id']. "/".$user['brokerId'], '<span class="label label-default">' . lang('brokerage_inactive') . '</span>'); ?></td>
                                        <td>
                                            <?php echo anchor('company/update_company_view/' . $user['companyId'], lang('actions_edit')); ?> | 
                                            <?php echo anchor('ixsolutions_admin/brokerage/profile/' . $user['id'], lang('actions_see')); ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
