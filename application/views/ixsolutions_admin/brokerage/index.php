<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo anchor('ixsolutions_admin/brokerage/inviteBroker', '<i class="fa fa-plus"></i> ' . lang('invite_broker'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th><?php echo lang('brokerage_UsersName'); ?></th>
                                    <th><?php echo lang('brokerage_email'); ?></th>
                                    <th><?php echo lang('brokerage_status'); ?></th>
                                    <th><?php echo lang('brokerage_action'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($records as $user): ?>
                                    <tr>
                                        <td>
                                            <?php echo anchor('ixsolutions_admin/brokerage/companyList/' . $user['brokerId'], $user['first_name']. " ".$user['last_name']); ?>
                                        </td>
                                        <td><?php echo $user['email']; ?></td>
                                        <td><?php echo ($user['active']) ? anchor('ixsolutions_admin/brokerage/deactivate/' .$user['id'] , '<span class="label label-success">' . lang('brokerage_active') . '</span>') : anchor('ixsolutions_admin/brokerage/activate/' . $user['id'], '<span class="label label-default">' . lang('brokerage_inactive') . '</span>'); ?></td>
                                        <td>
                                            <?php echo anchor('ixsolutions_admin/brokerage/profile/' . $user['id'], lang('actions_see')); ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
