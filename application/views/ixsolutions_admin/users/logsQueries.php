<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">User Log Queries</h3>
                        <div class="pull-right">
                            <span class="clickable filter" data-toggle="tooltip" title="Toggle table filter" data-container="body">
                                <i class="glyphicon glyphicon-filter" id='search'>Search</i> 
                            </span>
                        </div>
                    </div>
                    <div class="panel-body">
                        <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter Log Details" onkeyup="searchTable(this.value);"/>
                    </div>
                    <table class="table table-striped table-hover" id="dev-table">
                        <thead>
                            <tr>
                                <th><?php echo lang('users_userName'); ?></th>
                                <th><?php echo lang('users_email'); ?></th>
                                <th><?php echo lang('users_activity'); ?></th>
                                <th><?php echo lang('users_queryString'); ?></th>
                                <th><?php echo lang('users_downloadTime'); ?></th>
                                <th><?php echo lang('users_downloadFile'); ?></th>
                                <th><?php echo lang('users_status'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $count = 0; foreach ($users as $key => $user): ?>
                                <tr>
                                    <?php if ($user['logInTime'] == '') : ?>
                                        <td> 
                                            <?php echo anchor('ixsolutions_admin/users/profile/' . $user['id'], $user['first_name'] . " " . $user['last_name']); ?>
                                        </td>
                                        <td><?php echo htmlspecialchars($user['email'], ENT_QUOTES, 'UTF-8'); ?></td>
                                        <td><?php echo htmlspecialchars($user['activity'], ENT_QUOTES, 'UTF-8'); ?></td>
                                        <td><?php echo htmlspecialchars((preg_match('#\b(INSERT|DELETE|UPDATE)\b#',$user['query_string'])? '---' : $user['query_string']), ENT_QUOTES, 'UTF-8'); ?></td>
                                        <td><?php echo htmlspecialchars((($user['downloadTime'])?$user['downloadTime'] : '---'), ENT_QUOTES, 'UTF-8'); ?></td>
                                        <td><?php echo htmlspecialchars((($user['downloadFile'])?$user['downloadFile'] : '---'), ENT_QUOTES, 'UTF-8'); ?></td>
                                        <td><?php echo ($user['active']) ? anchor('ixsolutions_admin/users/deactivate/' . $user['id'], '<span class="label label-success">' . lang('users_active') . '</span>') : anchor('ixsolutions_admin/users/activate/' . $user['id'], '<span class="label label-default">' . lang('users_inactive') . '</span>'); ?></td>
                                    <?php endif; ?>
                                </tr>
                                <?php $count++;endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
<link rel="stylesheet" href="<?php echo base_url() . "style/main.css"; ?>">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>
<script language = "JavaScript" type = "text/javascript" script src = "http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.0/jquery-ui.min.js" ></script>
<script>
$(document).ready(function () {
    $("#search").click(function () {
        var $this = $(this),
                $panel = $this.parents('.panel');
        $panel.find('.panel-body').slideToggle();
        if ($this.css('display') != 'none') {
            $panel.find('.panel-body input').focus();
        }
    });
});
function searchTable(inputVal)
{
    var table = $('#dev-table');
    table.find('tr').each(function (index, row)
    {
        var allCells = $(row).find('td');
        if (allCells.length > 0)
        {
            var found = false;
            allCells.each(function (index, td)
            {
                var regExp = new RegExp(inputVal, 'i');
                if (regExp.test($(td).text()))
                {
                    found = true;
                    return false;
                }
            });
            if (found == true)
                $(row).show();
            else
                $(row).hide();
        }
    });
}
</script>