<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo anchor('admin/product/create', '<i class="fa fa-plus"></i> ' . lang('products_create_product'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th><?php echo lang('products_name'); ?></th>
                                <th><?php echo lang('product_description'); ?></th>
                                <th><?php echo lang('product_image'); ?></th>
                                <th><?php echo lang('products_status'); ?></th>
                                <th><?php echo lang('products_action'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($products as $product): ?>
                                <tr>
                                    <td><?php echo htmlspecialchars($product['name'], ENT_QUOTES, 'UTF-8'); ?></td>
                                    <td><?php echo htmlspecialchars($product['description'], ENT_QUOTES, 'UTF-8'); ?></td>
                                    <td><?php echo htmlspecialchars($product['productImage'], ENT_QUOTES, 'UTF-8'); ?></td>
                                    <td><?php echo ($product['status']) ? anchor('admin/product/deactivate/' . $product['productId'], '<span class="label label-success">' . lang('products_active') . '</span>') : anchor('admin/product/activate/' . $product['productId'], '<span class="label label-default">' . lang('products_inactive') . '</span>'); ?></td>
                                    <td>
                                        <?php echo anchor('admin/product/edit/' . $product['productId'], lang('actions_edit')); ?>
<!--                                        <a href="--><?php //echo  'product/delete/' . $product['productId'] ?><!--" onclick="return confirm('Are you sure you want to delete this item?');" >Delete</a>-->
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>