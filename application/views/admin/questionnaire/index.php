<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo anchor('admin/questionnaire/create', '<i class="fa fa-plus"></i> ' . lang('questionnaire_create_questionnaire'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th><?php echo lang('product_name'); ?></th>
                                <th><?php echo lang('questionnaire_question'); ?></th>
                                <th><?php echo lang('questionnaire_status'); ?></th>
                                <th><?php echo lang('products_action'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($questions as $question): ?>
                                <tr>
                                    <td><?php echo htmlspecialchars($question['product']['name'], ENT_QUOTES, 'UTF-8'); ?></td>
                                    <td><?php echo htmlspecialchars($question['question'], ENT_QUOTES, 'UTF-8'); ?></td>
                                    <td><?php echo ($question['status']) ? anchor('admin/questionnaire/deactivate/' . $question['id'], '<span class="label label-success">' . lang('questionnaire_active') . '</span>') : anchor('admin/questionnaire/activate/' . $question['id'], '<span class="label label-default">' . lang('questionnaire_inactive') . '</span>'); ?></td>
                                    <td>
                                        <?php echo anchor('admin/questionnaire/edit/' . $question['id'], lang('actions_edit')); ?>
<!--                                        <a href="--><?php //echo  'questionnaire/delete/' . $question['id'] ?><!--" onclick="return confirm('Are you sure you want to delete this item?');" >Delete</a>-->
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>