<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="<?php echo base_url(); ?>bootstrap/js/bootstrap-datepicker.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> 
<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <form class="form-horizontal" action="<?php echo site_url() ?>admin/Company/insert_company" method="post">
                            <?php
                            if (!empty($company_detail)) {
                                ?>
                                <input type="hidden" class="form-control" id="companyid" name="companyid" value="<?php echo $company_detail[0]->companyId ?>">
                                <input type="hidden" class="form-control" id="companyid" name="userid" value="<?php echo $company_detail[0]->id ?>">
                                <h4>Broker Info</h4><hr>
                                <div class="form-group">
                                    <label for="brokerage_name" class="col-sm-2 control-label">Brokerage Name:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="brokerage_name" name="brokerage_name" value="<?php echo $company_detail[0]->brokerage_name ?>" placeholder="Brokerage Name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="bfname" class="col-sm-2 control-label">Broker First Name:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="bfname" name="bfname" value="<?php echo $company_detail[0]->broker_fname ?>" placeholder="Broker First Name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="blname" class="col-sm-2 control-label">Broker Last Name:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="blname" name="blname" value="<?php echo $company_detail[0]->broker_lname ?>" placeholder="Broker Last Name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="npn" class="col-sm-2 control-label">NPN #:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="npn" name="npn" value="<?php echo $company_detail[0]->npn ?>" placeholder="NPN #" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="col-sm-2 control-label">Phone #:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $company_detail[0]->phone ?>" placeholder="Phone #" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="bemail" class="col-sm-2 control-label">Email:</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="bemail" name="bemail" value="<?php echo $company_detail[0]->bemail ?>" placeholder="Email" required>
                                    </div>
                                </div>
                                <hr> <h4>Company Info</h4><hr>
                                <div class="form-group">
                                    <label for="cname" class="col-sm-2 control-label">Company Name:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="cname" name="cname" value="<?php echo $company_detail[0]->company ?>" placeholder="Company Name" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="address" class="col-sm-2 control-label">Address:</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" id="address" name="address" Placeholder="Address"><?php echo $company_detail[0]->address ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="zipcode" class="col-sm-2 control-label">Zipcode:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="zipcode" name="zipcode" Placeholder="Zipcode" value="<?php echo $company_detail[0]->zipcode ?>" required onchange="getValues(this.value);">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="city" class="col-sm-2 control-label">City:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="city" name="city" value="<?php echo $company_detail[0]->city ?>" Placeholder="City" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="country" class="col-sm-2 control-label">County:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="country" name="country" value="<?php echo $company_detail[0]->country ?>" Placeholder="County" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="state" class="col-sm-2 control-label">State:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="state" name="state" value="<?php echo $company_detail[0]->state ?>" Placeholder="State" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-sm-2 control-label">Email Id:</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="email" name="email" value="<?php echo $company_detail[0]->email ?>" Placeholder="Email Id" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="no_of_emp" class="col-sm-2 control-label">No of Employee</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" id="no_of_emp" name="no_of_emp" value="<?php echo $company_detail[0]->no_of_emp ?>" Placeholder="No of Employee" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="sic" class="col-sm-2 control-label">SIC Code:</label>
                                    <div class="col-sm-10">
                                        <select class="form-control itemName" id="sic" name="sic" required>
                                            <option value="">SIC Code</option>
                                            <?php
                                            foreach ($sic_code as $sc) {
                                                ?>
                                                <option value="<?php echo $sc->sicId ?>" <?php
                                                if ($company_detail[0]->sicId == $sc->sicId) {
                                                    echo "selected";
                                                }
                                                ?>><?php echo $sc->code . " " . $sc->sicText ?></option>
                                                        <?php
                                                    }
                                                    ?>

                                        </select>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class='col-sm-offset-2 col-sm-10'>
                                        <button class="btn btn-primary btn-flat" type="submit" id="add" name="add">Submit</button>
                                        <button class="btn btn-warning btn-flat" type="reset">Reset</button>
                                        <a class="btn btn-default btn-flat" href="">Cancel</a>
                                    </div>
                                </div>
                                <?php
                            } else {
                                ?>
                                <h4>Broker Info</h4><hr>
                                <div class="form-group">
                                    <label for="brokerage_name" class="col-sm-2 control-label">Brokerage Name:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="brokerage_name" name="brokerage_name" value="" placeholder="Brokerage Name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="bfname" class="col-sm-2 control-label">Broker First Name:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="bfname" name="bfname" value="" placeholder="Broker First Name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="blname" class="col-sm-2 control-label">Broker Last Name:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="blname" name="blname" value="" placeholder="Broker Last Name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="npn" class="col-sm-2 control-label">NPN #:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="npn" name="npn" value="" placeholder="NPN #" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="col-sm-2 control-label">Phone #:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="phone" name="phone" value="" placeholder="Phone #" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="bemail" class="col-sm-2 control-label">Email:</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="bemail" name="bemail" value="" placeholder="Email" required>
                                    </div>
                                </div>
                                <hr> <h4>Company Info</h4><hr>
                                <div class="form-group">
                                    <label for="cname" class="col-sm-2 control-label">Company Name:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="cname" name="cname" placeholder="Company Name" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="address" class="col-sm-2 control-label">Address:</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" id="address" name="address" Placeholder="Address"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="zipcode" class="col-sm-2 control-label">Zipcode:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="zipcode" name="zipcode" Placeholder="Zipcode" required onchange="getValues(this.value);">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="city" class="col-sm-2 control-label">City:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="city" name="city" Placeholder="City" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="country" class="col-sm-2 control-label">County:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="country" name="country" Placeholder="County" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="state" class="col-sm-2 control-label">State:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="state" name="state"  Placeholder="State" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-sm-2 control-label">Email Id:</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="email" name="email" Placeholder="Email Id" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="no_of_emp" class="col-sm-2 control-label">No of Employee</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" id="no_of_emp" name="no_of_emp" Placeholder="No of Employee" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="sic" class="col-sm-2 control-label">SIC Code:</label>
                                    <div class="col-sm-10">
                                        <select class="form-control itemName" id="sic" name="sic" required>
                                            <option value="">SIC Code</option>
                                            <?php
                                            foreach ($sic_code as $sc) {
                                                ?>
                                                <option value="<?php echo $sc->sicId ?>" ><?php echo $sc->code . " " . $sc->sicText ?></option>
                                                <?php
                                            }
                                            ?>

                                        </select>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="product" class="col-sm-2 control-label">Product</label>
                                    <div class="col-sm-10">
                                        <?php
                                        foreach ($product as $p) {
                                            ?>
                                            <input type="checkbox" id="product" name="product[]" value="<?php echo $p->productId ?>" class="">&nbsp; <?php echo $p->description; ?><br>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group" id="offer" hidden>
                                    <label for="offer_emp" class="col-sm-2 control-label">Currently offering to Employee</label>
                                    <div class="col-sm-10">
                                        <input type="checkbox" id="offer_emp" name="offer_emp" value="">
                                    </div>
                                </div>
                                <div class="form-group" id="dates" hidden>
                                    <label for="renewal_date" class="col-sm-2 control-label">Renewal Date</label>
                                    <div class="col-sm-10">
                                        <input type="text" id="renewal_date" name="renewal_date" value="" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class='col-sm-offset-2 col-sm-10'>
                                        <button class="btn btn-primary btn-flat" type="submit" id="add" name="add" value="add">Submit</button>
                                        <button class="btn btn-warning btn-flat" type="reset">Reset</button>
                                        <a class="btn btn-default btn-flat" href="">Cancel</a>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url() . "scripts/datePicker.js"; ?>"></script>
<link rel="stylesheet" href="<?php echo base_url() . "style/datepicker.css"; ?>"/>
<script>
function getValues(zipcode) {
    $.ajax({
        url: '<?= base_url(); ?>admin/Company/getCountryStateCity',
        data: {"zipcode": zipcode},
        success: function (result) {
            var count_State_City = JSON.parse(result);
            $('#country').val(count_State_City.country);
            $('#state').val(count_State_City.state);
            $('#city').val(count_State_City.city);
            console.log(count_State_City);
        }
    });
}
</script>
<script>
    $('.itemName').select2();
</script>
<script>
    $(document).ready(function () {
        var checkboxes = $('input[type=checkbox]');
        $(checkboxes).on('change', function () {
            if ($(checkboxes).is(':checked')) {
                $('#offer').show();
            }
            else
            {
                $('#offer').hide();
                $('#dates').hide();
            }

        });
        $('#offer_emp').change(function () {
            if ($('#offer_emp').is(":checked"))
                $('#dates').show();
            else
                $('#dates').hide();
        });
    });
</script>
<script>
    var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
    $('#renewal_date').datepicker({
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
    });
</script>