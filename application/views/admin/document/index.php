<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo anchor('admin/document/create', '<i class="fa fa-plus"></i> ' . lang('document_create_document'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th><?php echo lang('document_name'); ?></th>
                                <th><?php echo lang('document_status'); ?></th>
                                <th><?php echo lang('document_action'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($documents as $document): ?>
                                <tr>
                                    <td><?php echo htmlspecialchars($document['documentFile'], ENT_QUOTES, 'UTF-8'); ?></td>
                                    <td><?php echo ($document['status']) ? anchor('admin/document/deactivate/' . $document['documentId'], '<span class="label label-success">' . lang('document_active') . '</span>') : anchor('admin/document/activate/' . $document['documentId'], '<span class="label label-default">' . lang('document_inactive') . '</span>'); ?></td>
                                    <td>
                                        <?php echo anchor('admin/document/edit/' .  $document['documentId'], lang('actions_edit')); ?>
<!--                                        <a href="--><?php //echo  'product/delete/' . $product['productId'] ?><!--" onclick="return confirm('Are you sure you want to delete this item?');" >Delete</a>-->
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>