<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
<div class="content">
    <div class="row">
         <div class="col-md-12">
             <div class="box">
                    <div class="box-body">
            <form class="form-horizontal" action="<?php echo site_url()?>admin/Company/question_answer_upload_view" method="post">
               <input type="hidden" id="cid" name="cid" value="<?php echo $cid?>">
           
                   <div class="form-group">
                  <div class='col-sm-offset-2 col-sm-10'>
                        <button type="submit" class="btn btn-primary btn-flat">Upload File</button>
                    </div>
              
                </div>
            </form>
            <form class="form-horizontal" action="<?php echo site_url()?>admin/Company/insert_question_answer" method="post">
                <input type="hidden" id="cid" name="cid" value="<?php echo $cid?>">
             <?php
               $no=1;
               foreach($question as $q)
               {
               ?>
                <div class="form-group">
                    <input type="hidden" id="qid<?php echo $no?>" name="qid<?php echo $no?>" value="<?php echo $q->id?>">
                    <label for="" class="col-sm-2 control-label"><?php echo $no.". ".$q->question;?></label>
                     <div class="col-sm-10">
                    <?php
                    foreach($answer as $a)
                    {
                        if($a->question_id==$q->id)
                        {
                            if($q->question_type=='radio')
                            {
                    ?>
                   
                        <input type="radio"  name="aid<?php echo $no?>[]" id="aid<?php echo $no?>" value="<?php echo $a->id?>"><?php echo $a->answer ?>                    
                <?php
                        }
                        else 
                        {
                      ?>
                   
                        <input type="checkbox" name="aid<?php echo $no?>[]" id="aid<?php echo $no?>" value="<?php echo $a->id?>"><?php echo $a->answer ?>                    
                <?php       
                        }
                        }
                     }
                     $no++;
                ?>
                      </div> 
                </div>
                <?php
                }
                ?>
                <input type="hidden" id="cnt" name="cnt" value="<?php echo $no?>">
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Do you have any question?</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="ques_answer<?php echo ($no-1);?>" id="ques_answer<?php echo ($no-1);?>" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                  <div class='col-sm-offset-2 col-sm-10'>
                        <button class="btn btn-primary btn-flat" type="submit">Submit</button>
                        <button class="btn btn-warning btn-flat" type="reset">Reset</button>
                        <a class="btn btn-default btn-flat" href="">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
        </div>
    </div>
    </div>
</div>
</div>