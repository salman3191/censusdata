<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
<div class="content">
    <div class="row">
         <div class="col-md-12">
             <div class="box">
                    <div class="box-body">  
            <form class="form-horizontal" action="<?php echo site_url()?>admin/Company/insert_question_answer" method="post" enctype="multipart/form-data">
               <input type="text" id="cid" name="cid" value="<?php echo $cid?>">
                <div class="form-group">
                    <label for="file_type" class="col-sm-2 control-label">File Type</label>
                    <div class="col-sm-10">
                        <select name="file_type" id="file_type" class="form-control" required>
                            <option value="">File Type</option>
                           <?php
                           foreach($document as $doc)
                           {
                           ?>
                            <option value="<?php echo $doc->documentId?>"><?php echo $doc->documentFile?></option>
                            <?php
                           }
                           ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="file_name" class="col-sm-2 control-label">File</label>
                    <div class="col-sm-10">
                        <input type="file" name="file_name" id="file_name" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class='col-sm-offset-2 col-sm-10'>
                        <button class="btn btn-primary btn-flat" type="submit">Submit</button>
                        <button class="btn btn-warning btn-flat" type="reset">Reset</button>
                        <a class="btn btn-default btn-flat" href="">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
        </div>
    </div>
</div>
</div>
</div>