<div class="container">
    <div class="row">
        <div class="col-md-6" style="background: black; color:whitesmoke">
            <h3>Census
                <a href="#tab1" data-toggle="tab">
                    <span class="glyphicon glyphicon-home"></span> Home</a>
            </h3>
        </div>
        <div class="col-md-6 align-right" style="background: black; color:whitesmoke">
            <h3><a href="<?php echo site_url("login") ?>">LogIn</a> / <a href="<?php echo site_url("register") ?>">Register</a></h3>
        </div>
        <hr>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <hr>
            <form method="POST" action="<?php echo base_url(); ?>ixsolutions_admin/users/register_user" id="registrationForm">
                <div class="form-group">
                    <label for="email" class="col-md-3 label-heading asterisk">User Type</label>
                    <div class="col-md-9">
                        <select class="form-control" name="dbUserType">   
                            <option selected value="0">--Select User Type--</option>
                        <?php foreach($userType as $value) : ?>
                            <?php if($value['id']!=1) : ?>
                                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                            <?php endif; ?>
                        <?php endforeach;?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                        <label for="email" class="col-md-3 label-heading asterisk">Email</label>
                    <div class="col-md-9">
                        <input type="email" class="form-control" id="email-in" name="txtEmail" value="" required="required">
                    </div>
                </div>
                <div class="form-group">
                        <label for="firstName" class="col-md-3 label-heading asterisk">First Name</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="email-in" name="txtFirstName" value="" required="required">
                    </div>
                </div>
                <div class="form-group">
                        <label for="lastName" class="col-md-3 label-heading asterisk">Last Name</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="email-in" name="txtLastName" value="" required="required">
                    </div>
                </div>
                <div class="form-group">
                        <label for="passWord" class="col-md-3 label-heading asterisk">Password</label>
                    <div class="col-md-9">
                        <input type="password" class="form-control" id="email-in" name="txtPassword" value="" required="required">
                    </div>
                </div>
                <div class="form-group">
                        <label for="confirmPassword" class="col-md-3 label-heading asterisk">Confirm Password</label>
                    <div class="col-md-9">
                        <input type="password" class="form-control" id="email-in" name="txtConfirmPassword" value="" required="required">
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <div class="col-md-12" style="padding:center;">
                        <input type="submit" name="submit" class="btn btn-primary form-control" value="Save" />
                    </div>  
                </div>
            </form>
            <hr>
        </div>
    </div>
</div>