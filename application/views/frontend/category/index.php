<div class="container marketing">
    <!-- Three columns of text below the carousel -->
    <div class="row">
        <?php if($this->session->flashdata('message')){?>
            <div class="<?php echo $this->session->flashdata('class')?>">
                <?php echo $this->session->flashdata('message')?>
            </div>
        <?php } ?>
        <div class="categories" >
            <div class="col-md-4 col-sm-6 col-lg-3 categories">
                <a style="width:100%;position:relative;top:0;left:0;display:inline-block;margin-top:10px;margin-bottom:10px;overflow:hidden;"
                    href="<?php echo base_url() ."offers/filter?categories=beauty-health"?>">
                    <span style="display: block; height: 0px; padding: 0px 0px 100%;
                    background-image: url(&quot;https://snagshoutprod.s3.amazonaws.com/1433186425B%26H-CUP.jpg&quot;);
                    transform: scale(1); background-repeat: no-repeat; background-position: center center; background-size: cover;
                    position: relative; text-align: center; transition: all 0.3s ease 0s;">
                    </span>
                    <div style="width: 100%; height: 100%; position: absolute; top: 0px; left: 0px; text-align: center;
                     background: rgba(0, 0, 0, 0.2) none repeat scroll 0% 0%; transition: all 0.3s ease 0s;"
                        class="overlay"><h2 class="title" style="color:white;">Beauty &amp; Health</h2>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-lg-3 categories">
                <a style="width:100%;position:relative;top:0;left:0;display:inline-block;margin-top:10px;margin-bottom:10px;overflow:hidden;"
                   href="<?php echo base_url() ."offers/filter?categories=car-automotive"?>">
                        <span style="display: block; height: 0px; padding: 0px 0px 100%;
                        background-image: url(&quot;https://s3.amazonaws.com/snagshoutprod/1459196346cars.jpg&quot;);
                        transform: scale(1); background-repeat: no-repeat; background-position: center center; background-size: cover;
                        position: relative; text-align: center; transition: all 0.3s ease 0s;">
                        </span>
                    <div style="width: 100%; height: 100%; position: absolute; top: 0px; left: 0px; text-align: center;
                         background: rgba(0, 0, 0, 0.2) none repeat scroll 0% 0%; transition: all 0.3s ease 0s;"
                         class="overlay"><h2 class="title" style="color:white;">Cars &amp; Automotive</h2>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-lg-3 categories">
                <a style="width:100%;position:relative;top:0;left:0;display:inline-block;margin-top:10px;margin-bottom:10px;overflow:hidden;"
                   href="<?php echo base_url() ."offers/filter?categories=clothing-shoes-jewlery"?>">
                        <span style="display: block; height: 0px; padding: 0px 0px 100%;
                        background-image: url(&quot;https://snagshoutprod.s3.amazonaws.com/1433187125CLOTH-SHIRTS-small.jpg&quot;);
                        transform: scale(1); background-repeat: no-repeat; background-position: center center; background-size: cover;
                        position: relative; text-align: center; transition: all 0.3s ease 0s;">
                        </span>
                    <div style="width: 100%; height: 100%; position: absolute; top: 0px; left: 0px; text-align: center;
                         background: rgba(0, 0, 0, 0.2) none repeat scroll 0% 0%; transition: all 0.3s ease 0s;"
                         class="overlay"><h2 class="title" style="color:white;">Clothing, Shoes &amp; Jewelry</h2>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-lg-3 categories">
                <a style="width:100%;position:relative;top:0;left:0;display:inline-block;margin-top:10px;margin-bottom:10px;overflow:hidden;"
                   href="<?php echo base_url() ."offers/filter?categories=electronics-computers"?>">
                        <span style="display: block; height: 0px; padding: 0px 0px 100%;
                        background-image: url(&quot;https://snagshoutprod.s3.amazonaws.com/1433186558ELECTRONIC-DESK1-small.jpg&quot;);
                        transform: scale(1); background-repeat: no-repeat; background-position: center center; background-size: cover;
                        position: relative; text-align: center; transition: all 0.3s ease 0s;">
                        </span>
                    <div style="width: 100%; height: 100%; position: absolute; top: 0px; left: 0px; text-align: center;
                         background: rgba(0, 0, 0, 0.2) none repeat scroll 0% 0%; transition: all 0.3s ease 0s;"
                         class="overlay"><h2 class="title" style="color:white;">Electronics &amp; Computers</h2>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-lg-3 categories">
                <a style="width:100%;position:relative;top:0;left:0;display:inline-block;margin-top:10px;margin-bottom:10px;overflow:hidden;"
                   href="<?php echo base_url() ."offers/filter?categories=grocery"?>">
                        <span style="display: block; height: 0px; padding: 0px 0px 100%;
                        background-image: url(&quot;https://snagshoutprod.s3.amazonaws.com/1433187434GROCERY-COOKING-small.jpg&quot;);
                        transform: scale(1); background-repeat: no-repeat; background-position: center center; background-size: cover;
                        position: relative; text-align: center; transition: all 0.3s ease 0s;">
                        </span>
                    <div style="width: 100%; height: 100%; position: absolute; top: 0px; left: 0px; text-align: center;
                         background: rgba(0, 0, 0, 0.2) none repeat scroll 0% 0%; transition: all 0.3s ease 0s;"
                         class="overlay"><h2 class="title" style="color:white;">Grocery</h2>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-lg-3 categories">
                <a style="width:100%;position:relative;top:0;left:0;display:inline-block;margin-top:10px;margin-bottom:10px;overflow:hidden;"
                   href="<?php echo base_url() ."offers/filter?categories=home-garden-tools"?>">
                        <span style="display: block; height: 0px; padding: 0px 0px 100%;
                        background-image: url(&quot;https://snagshoutprod.s3.amazonaws.com/1433187570H%26G-TOOLS.jpg&quot;);
                        transform: scale(1); background-repeat: no-repeat; background-position: center center; background-size: cover;
                        position: relative; text-align: center; transition: all 0.3s ease 0s;">
                        </span>
                    <div style="width: 100%; height: 100%; position: absolute; top: 0px; left: 0px; text-align: center;
                         background: rgba(0, 0, 0, 0.2) none repeat scroll 0% 0%; transition: all 0.3s ease 0s;"
                         class="overlay"><h2 class="title" style="color:white;">Home, Garden &amp; Tools</h2>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-lg-3 categories">
                <a style="width:100%;position:relative;top:0;left:0;display:inline-block;margin-top:10px;margin-bottom:10px;overflow:hidden;"
                   href="<?php echo base_url() ."offers/filter?categories=kids-baby"?>">
                        <span style="display: block; height: 0px; padding: 0px 0px 100%;
                        background-image: url(&quot;https://snagshoutprod.s3.amazonaws.com/1433187892kids-small.jpg&quot;);
                        transform: scale(1); background-repeat: no-repeat; background-position: center center; background-size: cover;
                        position: relative; text-align: center; transition: all 0.3s ease 0s;">
                        </span>
                    <div style="width: 100%; height: 100%; position: absolute; top: 0px; left: 0px; text-align: center;
                         background: rgba(0, 0, 0, 0.2) none repeat scroll 0% 0%; transition: all 0.3s ease 0s;"
                         class="overlay"><h2 class="title" style="color:white;">Kids &amp; Baby</h2>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-lg-3 categories">
                <a style="width:100%;position:relative;top:0;left:0;display:inline-block;margin-top:10px;margin-bottom:10px;overflow:hidden;"
                   href="<?php echo base_url() ."offers/filter?categories=media"?>">
                        <span style="display: block; height: 0px; padding: 0px 0px 100%;
                        background-image: url(&quot;https://snagshoutprod.s3.amazonaws.com/1433186682ELECTRONIC-PSP-small.jpg&quot;);
                        transform: scale(1); background-repeat: no-repeat; background-position: center center; background-size: cover;
                        position: relative; text-align: center; transition: all 0.3s ease 0s;">
                        </span>
                    <div style="width: 100%; height: 100%; position: absolute; top: 0px; left: 0px; text-align: center;
                         background: rgba(0, 0, 0, 0.2) none repeat scroll 0% 0%; transition: all 0.3s ease 0s;"
                         class="overlay"><h2 class="title" style="color:white;">Media</h2>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-lg-3 categories">
                <a style="width:100%;position:relative;top:0;left:0;display:inline-block;margin-top:10px;margin-bottom:10px;overflow:hidden;"
                   href="<?php echo base_url() ."offers/filter?categories=officesupplies"?>">
                        <span style="display: block; height: 0px; padding: 0px 0px 100%;
                        background-image: url(&quot;https://s3.amazonaws.com/snagshoutprod/1459196247office-supplies.jpg&quot;);
                        transform: scale(1); background-repeat: no-repeat; background-position: center center; background-size: cover;
                        position: relative; text-align: center; transition: all 0.3s ease 0s;">
                        </span>
                    <div style="width: 100%; height: 100%; position: absolute; top: 0px; left: 0px; text-align: center;
                         background: rgba(0, 0, 0, 0.2) none repeat scroll 0% 0%; transition: all 0.3s ease 0s;"
                         class="overlay"><h2 class="title" style="color:white;">Office Supplies</h2>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-lg-3 categories">
                <a style="width:100%;position:relative;top:0;left:0;display:inline-block;margin-top:10px;margin-bottom:10px;overflow:hidden;"
                   href="<?php echo base_url() ."offers/filter?categories=petsupplies"?>">
                        <span style="display: block; height: 0px; padding: 0px 0px 100%;
                        background-image: url(&quot;https://snagshoutprod.s3.amazonaws.com/14349924865635018628_866cd34a41_z.jpg&quot;);
                        transform: scale(1); background-repeat: no-repeat; background-position: center center; background-size: cover;
                        position: relative; text-align: center; transition: all 0.3s ease 0s;">
                        </span>
                    <div style="width: 100%; height: 100%; position: absolute; top: 0px; left: 0px; text-align: center;
                         background: rgba(0, 0, 0, 0.2) none repeat scroll 0% 0%; transition: all 0.3s ease 0s;"
                         class="overlay"><h2 class="title" style="color:white;">Pet Supplies</h2>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-lg-3 categories">
                <a style="width:100%;position:relative;top:0;left:0;display:inline-block;margin-top:10px;margin-bottom:10px;overflow:hidden;"
                   href="<?php echo base_url() ."offers/filter?categories=sports-outdoors"?>">
                        <span style="display: block; height: 0px; padding: 0px 0px 100%;
                        background-image: url(&quot;https://snagshoutprod.s3.amazonaws.com/1433187716S%26O-SURF-small.jpg&quot;);
                        transform: scale(1); background-repeat: no-repeat; background-position: center center; background-size: cover;
                        position: relative; text-align: center; transition: all 0.3s ease 0s;">
                        </span>
                    <div style="width: 100%; height: 100%; position: absolute; top: 0px; left: 0px; text-align: center;
                         background: rgba(0, 0, 0, 0.2) none repeat scroll 0% 0%; transition: all 0.3s ease 0s;"
                         class="overlay"><h2 class="title" style="color:white;">Sports &amp; Outdoors</h2>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-lg-3 categories">
                <a style="width:100%;position:relative;top:0;left:0;display:inline-block;margin-top:10px;margin-bottom:10px;overflow:hidden;"
                   href="<?php echo base_url() ."offers/filter?categories=toys"?>">
                        <span style="display: block; height: 0px; padding: 0px 0px 100%;
                        background-image: url(&quot;https://snagshoutprod.s3.amazonaws.com/1433187280TOYS-PACMAN-small.jpg&quot;);
                        transform: scale(1); background-repeat: no-repeat; background-position: center center; background-size: cover;
                        position: relative; text-align: center; transition: all 0.3s ease 0s;">
                        </span>
                    <div style="width: 100%; height: 100%; position: absolute; top: 0px; left: 0px; text-align: center;
                         background: rgba(0, 0, 0, 0.2) none repeat scroll 0% 0%; transition: all 0.3s ease 0s;"
                         class="overlay"><h2 class="title" style="color:white;">Toys</h2>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-lg-3 categories">
                <a style="width:100%;position:relative;top:0;left:0;display:inline-block;margin-top:10px;margin-bottom:10px;overflow:hidden;"
                   href="<?php echo base_url() ."offers/filter?categories=vitamins-suppliments"?>">
                        <span style="display: block; height: 0px; padding: 0px 0px 100%;
                        background-image: url(&quot;https://snagshoutprod.s3.amazonaws.com/14349967445581382256_fc266ff5f7_z.jpg&quot;);
                        transform: scale(1); background-repeat: no-repeat; background-position: center center; background-size: cover;
                        position: relative; text-align: center; transition: all 0.3s ease 0s;">
                        </span>
                    <div style="width: 100%; height: 100%; position: absolute; top: 0px; left: 0px; text-align: center;
                         background: rgba(0, 0, 0, 0.2) none repeat scroll 0% 0%; transition: all 0.3s ease 0s;"
                         class="overlay"><h2 class="title" style="color:white;">Vitamins &amp; Supplements</h2>
                    </div>
                </a>
            </div>
        </div>
    </div>