<footer style="background: #222222; position:absolute; bottom:0; width:100%; height:50px;">
    <div class="container" style="margin-top: 18px;">
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>
            <?php echo CB_COPYRIGHT; ?>
            <?php for ($nf = 0; $nf < count($navfoot); $nf++) : ?>
                &middot;
                <a href="<?php echo site_url($navfoot[$nf]['href']); ?>">
                    <?php echo $navfoot[$nf]['text'] ?>
                </a>
            <?php endfor; ?>
        </p>
    </div>
    
</footer>