<div class="container" >
    <div class="row">
        <div class="col-md-offset-2 col-md-9" style="margin-top: 10%;">  
            <form class="form-horizontal" action="<?php echo site_url()?>Frontend_company/insert_employee" method="post" enctype="multipart/form-data">
               <input type="hidden" id="cid" name="cid" value="<?php echo $company;?>">
                <input type="hidden" id="comp_emp" name="comp_emp" value="<?php echo $company_emp;?>">
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-3">
                    <a href="<?php echo site_url()?>Frontend_company/employee_upload_format"><label for="download_file" class="form-control  btn btn-default">Download Format</label></a>
                    </div>
                </div>
                <div class="form-group">
                    <label for="file_name" class="col-md-3 label-heading asterisk">Upload CSV</label>
                    <div class="col-md-8">
                        <input type="file" name="file_name" id="file_name" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-3">
                        <input class="form-control  btn btn-default" type="submit" name="add_file" id="add_file" value="Submit">
                    </div>
                </div>
            </form>
        </div>
        </div>
</div>