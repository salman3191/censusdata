<div class="container">
    <div class="row">
        <div class="col-md-offset-2 col-md-9" style="margin-top: 10%;"> 
            <form class="form-horizontal" action="<?php echo site_url()?>Frontend_company/question_answer_upload_view" method="post">
               <input type="hidden" id="cid" name="cid" value="<?php echo $cid?>">
                <div class="form-group">
                   <div class="form-group">
                  <div class='col-md-8 col-md-offset-3'>
                        <button type="submit" class="form-control btn btn-default">Upload File</button>
                    </div>
                </div>
                </div>
            </form>
            <form class="form-horizontal" action="<?php echo site_url()?>Frontend_company/insert_question_answer" method="post">
                <input type="hidden" id="cid" name="cid" value="<?php echo $cid?>">
             <?php
               $no=1;
               foreach($question as $q)
               {
               ?>
                <div class="form-group">
                    <input type="hidden" id="qid<?php echo $no?>" name="qid<?php echo $no?>" value="<?php echo $q->id?>">
                    <label for="" class="col-md-3 label-heading asterisk"><?php echo $no.". ".$q->question;?></label>
                     <div class="col-md-8">
                    <?php
                    foreach($answer as $a)
                    {
                        if($a->question_id==$q->id)
                        {
                            if($q->question_type=='radio')
                            {
                    ?>
                   
                        <input type="radio"  name="aid<?php echo $no?>[]" id="aid<?php echo $no?>" value="<?php echo $a->id?>"><?php echo $a->answer ?>                    
                <?php
                        }
                        else 
                        {
                      ?>
                   
                        <input type="checkbox" name="aid<?php echo $no?>[]" id="aid<?php echo $no?>" value="<?php echo $a->id?>"><?php echo $a->answer ?>                    
                <?php       
                        }
                        }
                     }
                     $no++;
                ?>
                      </div> 
                </div>
                <?php
                }
                ?>
                <input type="hidden" id="cnt" name="cnt" value="<?php echo $no?>">
                <div class="form-group">
                    <label for="" class="col-md-3 label-heading asterisk">do you have any question?</label>
                    <div class="col-md-8">
                        <textarea class="form-control" name="ques_answer<?php echo ($no-1);?>" id="ques_answer<?php echo ($no-1);?>" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                  <div class='col-md-8 col-md-offset-3'>
                        <button type="submit" class="form-control btn btn-default">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        </div>
    </div>
