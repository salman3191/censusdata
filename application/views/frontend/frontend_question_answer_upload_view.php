<div class="container" >
    <div class="row">
        <div class="col-md-offset-2 col-md-9" style="margin-top: 10%;">  
            <form class="form-horizontal" action="<?php echo site_url()?>Frontend_company/insert_question_answer" method="post" enctype="multipart/form-data">
               <input type="hidden" id="cid" name="cid" value="<?php echo $cid?>">
                <div class="form-group">
                    <label for="file_type" class="col-md-3 label-heading asterisk">File Type</label>
                    <div class="col-md-8">
                        <select name="file_type" id="file_type" class="form-control" required>
                            <option value="">File Type</option>
                           <?php
                           foreach($document as $doc)
                           {
                           ?>
                            <option value="<?php echo $doc->documentId?>"><?php echo $doc->documentFile?></option>
                            <?php
                           }
                           ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="file_name" class="col-md-3 label-heading asterisk">File</label>
                    <div class="col-md-8">
                        <input type="file" name="file_name" id="file_name" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-3">
                        <input class="form-control  btn btn-default" type="submit" name="add_file" id="add_file" value="Submit">
                    </div>
                </div>
            </form>
        </div>
        </div>
</div>