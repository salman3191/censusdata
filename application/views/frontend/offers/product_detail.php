<div class="container marketing">
    <!-- Three columns of text below the carousel -->
    <div class="row">
        <?php if($this->session->flashdata('message')){?>
            <div class="<?php echo $this->session->flashdata('class')?>">
                <?php echo $this->session->flashdata('message')?>
            </div>
        <?php } ?>
        <div class="col-md-8">
            <img src="<?php echo $marketing['img_src']; ?>">
        </div>
        <div class="col-md-4">
            <h3><?php echo $marketing['img_alt']; ?></h3>
            <span
                style="font-size: 28px; font-weight: bold;margin-bottom: 10px;margin-top: 20px;"><?php echo $marketing['price']; ?></span>
            <span
                style="font-size:18px;text-decoration:line-through; margin-bottom: 10px;margin-top: 20px;"><?php echo $marketing['retail_price']; ?></span>
            <hr/>
            <div class="col-xs-12">
                <div class="col-xs-4">
                    <div style="text-align:center;font-size:15px;">Discount :</div>
                    <div
                        style="text-align:center;font-size:18px;font-weight: bold;"><?php echo $marketing['discount'] . "%"; ?></div>
                </div>
                <div class="col-xs-4">
                    <div style="text-align:center;font-size:15px;">Days Left :</div>
                    <div
                        style="text-align:center;font-size:18px;font-weight: bold;"><?php echo $marketing['days_left']; ?></div>
                </div>
                <div class="col-xs-4">
                    <div style="text-align:center;font-size:15px;">In Stock :</div>
                    <div
                        style="text-align:center;font-size:18px;font-weight: bold;"><?php echo $marketing['coupon_left']; ?></div>
                </div>
            </div>

            <div class="col-xs-12">
                <a style="margin-bottom: 10px;margin-top: 20px;" target="_blank" href="<?php echo $marketing['href']; ?>"
                   class="btn-block btn btn-default">View on Amazon</a>
            </div>
            <div class="col-xs-12">
                <a style="margin-bottom: 10px;margin-top: 20px;" href="<?php echo $marketing['deal_link']; ?>"
                   class="btn-block btn btn-default">Grab Deal</a></div>
            <div class="col-xs-12">
                <p style="margin-bottom: 10px;margin-top: 20px;">Category
                    : <?php echo str_replace(" ", "-", ucwords(str_replace("-", " ", $marketing['category']))); ?></p>
            </div>
            <div class="col-xs-12">
                <p style="margin-bottom: 10px;margin-top: 20px;">Description
                    : <?php echo $marketing['description']; ?></p></div>
        </div>
    </div><!-- /.row -->