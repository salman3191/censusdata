<div class="container marketing">
    <!-- Three columns of text below the carousel -->
    <div class="row">
        <?php if($this->session->flashdata('message')){?>
            <div class="<?php echo $this->session->flashdata('class')?>">
                <?php echo $this->session->flashdata('message')?>
            </div>
        <?php } ?>
        <?php for ($m = 0; $m < count($marketing); $m++) : ?>
            <a href="<?php
            echo $marketing[$m]['href']; ?>">
            <div class="col-lg-3">
                <img class="img-circle" src="<?php
                echo $marketing[$m]['img_src']; ?>" alt="<?php
                echo $marketing[$m]['img_alt']; ?>" style="width: 140px; height: 140px;">
                <h4><?php echo $marketing[$m]['h2']; ?></h4>
                <p><?php echo $marketing[$m]['price']; ?></p>

            </div><!-- /.col-lg-4 -->
            </a>
        <?php endfor; ?>
    </div><!-- /.row -->