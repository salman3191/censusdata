<div class="container marketing">
    <div class="row">
        <?php if($this->session->flashdata('message')){?>
            <div class="<?php echo $this->session->flashdata('class')?>">
                <?php echo $this->session->flashdata('message')?>
            </div>
        <?php } ?>
        <div class="col-md-12">
            <h5 style="text-align: center">Please use the below coupon code</h5>
            <div style="padding: 10px; border: 1px solid #cccccc; height:235px;">
                <div class="col-md-12">
                    <div class="col-md-4" style="text-align: center;  border-right: 1px solid #cccccc;">
                        <p style="vertical-align: middle;"><img style="width:50%"
                                                                src="<?php echo $marketing['img_src']; ?>"></p>
                    </div>
                    <div class="col-md-8">
                        <div class="col-md-12" style="margin-top: 20px;">
                        <div class="col-md-3"><span style="font-size: 18px; font-weight: 200;"> Product Name : </span>
                        </div>
                        <div class="col-md-9"><span
                                style="font-size: 18px; "><?php echo $marketing['product_name']; ?></span>
                        </div>
                            </div>
                        <div class="col-md-12" style="margin-top: 20px; border-top: 1px solid #ccc;">
                            <div class="col-md-3" style="margin-top: 10px;"><span
                                    style="font-size: 18px; font-weight: 200;"> Coupon Code : </span>
                            </div>
                            <div class="col-md-9" style="margin-top: 10px;"><span
                                    style="font-size: 18px; "><?php echo $marketing['coupon_code']; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.row -->