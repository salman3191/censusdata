<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
    <section class="content-header">
        <?php echo $pagetitle; ?>
        <?php echo $breadcrumb; ?>
    </section>
<div class="content" >
    <div class="row">
         <div class="col-md-12">
             <div class="box">
                  <div class="box-header with-border">
                        <h3 class="box-title"><?php echo anchor('Company', '<i class="fa fa-plus"></i> Create Company' , array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                    </div>
                    <div class="box-body"> 
            <table class="table table-striped table-hover">
                <thead>
                    <th>Company</th>
                    <th>Email Address</th>
                    <th>Address</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Country</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    <?php
                    if(!empty($company_detail))
                    {
                    foreach($company_detail as $comp_detail)
                    {
                    ?>
                    <tr>
                        <td><?php echo $comp_detail->company?></td>
                        <td><?php echo $comp_detail->email?></td>
                        <td><?php echo $comp_detail->address?></td>
                        <td><?php echo $comp_detail->city?></td>
                        <td><?php echo $comp_detail->state?></td>
                        <td><?php echo $comp_detail->country?></td>
                        <td><a href="<?php echo site_url()?>Company/update_company_view/<?php echo $comp_detail->companyId?>">Edit</a>/<a href="<?php echo site_url()?>Company/export_company_detail/<?php echo $comp_detail->companyId?>">Export</a></td>
                    </tr>
                    <?php
                    }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
    </div>
</div>
</div>