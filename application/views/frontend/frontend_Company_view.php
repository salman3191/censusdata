<div class="container" >
    <div class="row">
        <div class="col-xs-offset-2 col-md-9" style="margin-top:10%;">
            <form class="form-horizontal" action="<?php echo site_url() ?>Frontend_company/insert_company" method="post">
              <?php
              if(!empty($company_detail))
              {
              ?>
                 <input type="hidden" id="companyid" name="companyid" value="<?php echo $company_detail[0]->companyId?>">
                 <input type="hidden" id="companyid" name="userid" value="<?php echo $company_detail[0]->id?>">
                 <h4>Broker Info</h4><hr>
                 <div class="form-group">
                    <label for="brokerage_name" class="col-md-3 label-heading asterisk">Brokerage Name:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="brokerage_name" name="brokerage_name" value="<?php echo $company_detail[0]->brokerage_name?>" placeholder="Brokerage Name" required>
                    </div>  
                </div>
                <div class="form-group">
                    <label for="bfname" class="col-md-3 label-heading asterisk">Broker First Name:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="bfname" name="bfname" value="<?php echo $company_detail[0]->broker_fname?>" placeholder="Broker First Name" required>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="blname" class="col-md-3 label-heading asterisk">Broker Last Name:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="blname" name="blname" value="<?php echo $company_detail[0]->broker_lname?>" placeholder="Broker Last Name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="npn" class="col-md-3 label-heading asterisk">NPN #:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="npn" name="npn" value="<?php echo $company_detail[0]->npn?>" placeholder="NPN #" required>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="phone" class="col-md-3 label-heading asterisk">Phone #:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $company_detail[0]->phone?>" placeholder="Phone #" required>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="bemail" class="col-md-3 label-heading asterisk">Email:</label>
                    <div class="col-md-8">
                        <input type="email" class="form-control" id="bemail" name="bemail" value="<?php echo $company_detail[0]->bemail?>" placeholder="Email" required>
                    </div>
                </div>
                 <hr> <h4>Company Info</h4><hr>
                 <div class="form-group">
                    <label for="cname" class="col-md-3 label-heading asterisk">Company Name:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="cname" name="cname" value="<?php echo $company_detail[0]->company?>" placeholder="Company Name" required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="address" class="col-md-3 label-heading asterisk">Address:</label>
                    <div class="col-md-8">
                        <textarea class="form-control" id="address" name="address" Placeholder="Address"><?php echo $company_detail[0]->address?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="zipcode" class="col-md-3 label-heading asterisk">Zipcode:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $company_detail[0]->zipcode?>" Placeholder="Zipcode" required onchange="getValues(this.value);">
                    </div>
                </div>
               <div class="form-group">
                    <label for="city" class="col-md-3 label-heading asterisk">City:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="city" name="city" value="<?php echo $company_detail[0]->city?>" Placeholder="City" required>
                    </div>
               </div>
               <div class="form-group">
                    <label for="country" class="col-md-3 label-heading asterisk">County:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="country" name="country" value="<?php echo $company_detail[0]->country?>" Placeholder="County" required>
                    </div>
                </div>
               <div class="form-group">
                    <label for="state" class="col-md-3 label-heading asterisk">State:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="state" name="state" value="<?php echo $company_detail[0]->state?>" Placeholder="State" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-md-3 label-heading asterisk">Email Id:</label>
                    <div class="col-md-8">
                        <input type="email" class="form-control" id="email" name="email" value="<?php echo $company_detail[0]->email?>" Placeholder="Email Id" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="no_of_emp" class="col-md-3 label-heading asterisk">No of Employee</label>
                    <div class="col-md-8">
                        <input type="number" class="form-control" id="no_of_emp" name="no_of_emp"  value="<?php echo $company_detail[0]->no_of_emp?>" required>
                  </div>
                </div>
                <div class="form-group">
                    <label for="sic" class="col-md-3 label-heading asterisk">SIC Code:</label>
                    <div class="col-md-8">
                        <select class="form-control itemName" id="sic" name="sic" required>
                            <option value="">SIC Code</option>
                            <?php
                            foreach ($sic_code as $sc) {
                                ?>
                                <option value="<?php echo $sc->sicId ?>" <?php if($company_detail[0]->sicId==$sc->sicId){ echo "selected"; }?>><?php echo $sc->code . " " . $sc->sicText ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class='col-md-8 col-md-offset-3'>
                        <button type="submit" class="form-control btn btn-default" id="add" name="add">Submit &  Next</button>
                    </div>
                </div>
              <?php
              }
              else
              {
              ?>
                 <h4>Broker Info</h4><hr>
                 <div class="form-group">
                    <label for="brokerage_name" class="col-md-3 label-heading asterisk">Brokerage Name:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="brokerage_name" name="brokerage_name" value="" placeholder="Brokerage Name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="bfname" class="col-md-3 label-heading asterisk">Broker First Name:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="bfname" name="bfname" value="" placeholder="Broker First Name" required>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="blname" class="col-md-3 label-heading asterisk">Broker Last Name:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="blname" name="blname" value="" placeholder="Broker Last Name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="npn" class="col-md-3 label-heading asterisk">NPN #:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="npn" name="npn" value="" placeholder="NPN #" required>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="phone" class="col-md-3 label-heading asterisk">Phone #:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="phone" name="phone" value="" placeholder="Phone #" required>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="bemail" class="col-md-3 label-heading asterisk">Email:</label>
                    <div class="col-md-8">
                        <input type="email" class="form-control" id="bemail" name="bemail" value="" placeholder="Email" required>
                    </div>
                </div>
                 <hr> <h4>Company Info</h4><hr>
                 
                 
                 <div class="form-group">
                    <label for="cname" class="col-md-3 label-heading asterisk">Company Name:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="cname" name="cname" value="" placeholder="Company Name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-md-3 label-heading asterisk">Address:</label>
                    <div class="col-md-8">
                        <textarea class="form-control" id="address" name="address" Placeholder="Address"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="zipcode" class="col-md-3 label-heading asterisk">Zipcode:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="zipcode" name="zipcode" Placeholder="Zipcode" required onchange="getValues(this.value);">
                    </div>
                </div>
                <div class="form-group">
                    <label for="city" class="col-md-3 label-heading asterisk">City:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="city" name="city" Placeholder="City" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="country" class="col-md-3 label-heading asterisk">County:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="country" name="country" Placeholder="County" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="state" class="col-md-3 label-heading asterisk">State:</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="state" name="state" Placeholder="State" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-md-3 label-heading asterisk">Email Id:</label>
                    <div class="col-md-8">
                        <input type="email" class="form-control" id="email" name="email" Placeholder="Email Id" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="no_of_emp" class="col-md-3 label-heading asterisk">No of Employee</label>
                    <div class="col-md-8">
                        <input type="number" class="form-control" id="no_of_emp" name="no_of_emp"  value="" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="sic" class="col-md-3 label-heading asterisk">SIC Code:</label>
                    <div class="col-md-8">
                        <select class="form-control itemName" id="sic" name="sic" required>
                            <option value="">SIC Code</option>
                            <?php
                            foreach ($sic_code as $sc) {
                                ?>
                                <option value="<?php echo $sc->sicId ?>"><?php echo $sc->code . " " . $sc->sicText ?></option>
                                <?php
                            }
                            ?>

                        </select>

                    </div>
                </div>
                <div class="form-group">
                    <label for="product" class="col-md-3 label-heading asterisk">Product</label>
                    <div class="col-md-8">
                    <?php
                    foreach ($product as $p) {
                        ?>
                         <input type="checkbox" id="product" name="product[]" value="<?php echo $p->productId ?>" class="form-control" ><?php echo $p->description; ?>
                     <?php
                        }
                        ?>
                    </div>
                </div>
                  <div class="form-group" id="offer" hidden>
                    <label for="offer_emp" class="col-md-3 label-heading asterisk">Currently offering to Employee</label>
                    <div class="col-md-8">
                        <input type="checkbox" id="offer_emp" name="offer_emp" value="" class="form-control" >
                    </div>
                </div>
                 <div class="form-group" id="dates" hidden>
                    <label for="renewal_date" class="col-md-3 label-heading asterisk">Renewal Date</label>
                    <div class="col-md-8">
                        <input type="text" id="renewal_date" name="renewal_date" value="" class="form-control" data-provide="datepicker-inline">
                    </div>
                </div>
                <div class="form-group">
                    <div class='col-md-8 col-md-offset-3'>
                        <button type="submit" class="form-control btn btn-default" id="add" name="add" value="add">Submit &  Next</button>
                    </div>
                </div>
             <?php
              }
              ?>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url() . "scripts/datePicker.js"; ?>"></script>
<link rel="stylesheet" href="<?php echo base_url() . "style/datepicker.css"; ?>"/>
<script>
    function getValues(zipcode) {
        $.ajax({
            url: '<?= base_url(); ?>Frontend_company/getCountryStateCity',
            data: {"zipcode": zipcode},
            success: function (result) {
                var count_State_City = JSON.parse(result);
                $('#country').val(count_State_City.country);
                $('#state').val(count_State_City.state);
                $('#city').val(count_State_City.city);
                console.log(count_State_City);
            }
        });
    }
</script>
<script>
$('.itemName').select2();    
</script>
<script>
$(document).ready(function() {
  var checkboxes = $('input[type=checkbox]');
  $(checkboxes).on('change', function() {
      if($(checkboxes).is(':checked')) {
           $('#offer').show();
      }
      else
       {
           $('#offer').hide();
           $('#dates').hide();
       }
    
  });
   $('#offer_emp').change(function(){
        if($('#offer_emp').is(":checked"))
            $('#dates').show();
        else
            $('#dates').hide();  
    });
});
</script>

<script>
$('#renewal_date').datepicker();
</script>