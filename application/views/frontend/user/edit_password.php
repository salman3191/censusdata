<div class="container marketing">
    <!-- Three columns of text below the carousel -->
    <div class="row">
        <?php if($this->session->flashdata('message')){?>
            <div class="<?php echo $this->session->flashdata('class')?>">
                <?php echo $this->session->flashdata('message')?>
            </div>
        <?php } ?>
        <div class="col-md-3" style="text-align: center; border-right:1px solid #ccc;">
            <div><img src="<?php echo $marketing['img_src']; ?>"></div>
            <div><h3><?php echo $marketing['user_name']; ?></h3></div>
            <div><p><?php echo $marketing['email']; ?></p></div>
            <div style="margin-top: 15px"><a href="<?php echo base_url() . "users/edit_profile" ?>"
                                             class="btn-block btn btn-default">Edit Profile</a></div>
            <div style="margin-top: 15px"><a href="<?php echo base_url() . "users/snag_history" ?>"
                                             class="btn-block btn btn-default">Snags History</a></div>
            <div style="margin-top: 15px"><a href="<?php echo base_url() . "users/edit_password" ?>"
                                             class="btn-block btn btn-default">Change Password</a></div>
        </div>
        <div class="col-md-9">
            <div style="padding: 10px;">
                <?php echo form_open_multipart(site_url("users/change_password_pro"), array("class" => "form-horizontal","id"=>"edit_user_password")) ?>
                    <div class="col-md-12 form-group">
                        <label class="col-md-3 control-label asterisk">Current Password</label>
                        <div class="col-md-9">
                            <input class="form-control" type="password" name="current_password" value="">
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <label class="col-md-3 control-label asterisk">New Password</label>
                        <div class="col-md-9">
                            <input class="form-control" type="password" name="new_pass1" value="">
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <label class="col-md-3 control-label asterisk">Confirm New Password</label>
                        <div class="col-md-9">
                            <input class="form-control" type="password" name="new_pass2" value="">
                        </div>
                    </div>
                    <div class="col-md-4 col-md-offset-5">
                        <input name="s" value="Change Password" class="btn btn-primary form-control" type="submit">
                    </div>
                <?php echo form_close() ?>
            </div>
        </div>
    </div><!-- /.row -->