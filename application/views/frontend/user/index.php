<div class="container marketing">
    <!-- Three columns of text below the carousel -->
    <div class="row">
        <?php if($this->session->flashdata('message')){?>
            <div class="<?php echo $this->session->flashdata('class')?>">
                <?php echo $this->session->flashdata('message')?>
            </div>
        <?php } ?>
        <div class="col-md-3" style="text-align: center; border-right:1px solid #ccc;">
            <div><img src="<?php echo $marketing['img_src']; ?>"></div>
            <div><h3><?php echo $marketing['user_name']; ?></h3></div>
            <div><p><?php echo $marketing['user_email']; ?></p></div>
            <div style="margin-top: 15px"><a href="<?php echo base_url() . "users/edit_profile" ?>"class="btn-block btn btn-default">Edit Profile</a></div>
            <div style="margin-top: 15px"><a href="<?php echo base_url() . "users/snag_history" ?>" class="btn-block btn btn-default">Snags History</a></div>
            <div style="margin-top: 15px"><a href="<?php echo base_url() . "users/edit_password" ?>" class="btn-block btn btn-default">Change Password</a></div>
        </div>
        <div class="col-md-9">
            <div style="padding: 10px; border: 1px solid #cccccc; height:235px;">
                <div class="col-md-5" style="text-align: center; border-right:1px solid #ccc;">
                    <h5>You have</h5>
                    <p style="vertical-align: middle;"><span
                            style="font-size: 45px; font-weight: 200;"><?php echo $marketing['snag_used']; ?></span><span
                            style="font-size: 24px; font-weight: 200;"> Snags Used</span></p>
                    <hr/>
                    <p style="vertical-align: middle;"><span
                            style="font-size: 45px; font-weight: 200;"><?php echo $marketing['snag_available']; ?></span><span
                            style="font-size: 24px; font-weight: 200;"> Snags Available</span></p>
                </div>
                <div class="col-md-7" style="text-align:center;height:215px;">
                    <h5>Deals</h5>
                    <h3 style="margin-top: 80px;"><a href="<?php echo base_url() . "offers" ?>">Grab More Deals!</a>
                    </h3>
                </div>
            </div>
        </div>
    </div><!-- /.row -->