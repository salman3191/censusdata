<div class="container marketing">
    <!-- Three columns of text below the carousel -->
    <div class="container ng-scope" ng-controller="userPersonalize">
        <?php if($this->session->flashdata('message')){?>
            <div class="<?php echo $this->session->flashdata('class')?>">
                <?php echo $this->session->flashdata('message')?>
            </div>
        <?php } ?>
        <?php echo form_open_multipart(site_url("users/add_amazon_link_to_account"), array("class" => "ng-pristine ng-valid form-horizontal","id"=>"amazon_link")) ?>
            <div class="row signup">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="card">
                        <div class="text-center">
                            <h2>One Last Thing!</h2>
                            <p>
                                You'll be snagging deals in no time.
                            </p>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <h4>Link Your Amazon Profile</h4>
                                <p>
                                    In order to use Snagshout you must link your Amazon profile to your Snagshout
                                    profile. This does not give us access to your Amazon account, it only allows us
                                    to verify that you are a legitimate Amazon shopper.
                                </p>

                                <p>
                                    Click <a href="https://www.amazon.com/gp/profile" target="_blank">this link</a>
                                    and, if prompted, login to Amazon. Once you arrive on your profile page, copy the url and
                                    paste it in the input below.
                                </p>

                                <div class="form-group">
                                    <label for="azUrl">Amazon Profile Url</label>
                                    <input class="form-control required" placeholder="https://www.amazon.com/gp/profile/A4BXSFGWDAXDLA" required="required" name="azUrl" autocomplete="off" type="text">
                                </div>
                                <hr>
                            </div>
                            <div class="col-xs-12">
                                <div class="signup-buttons" style="margin-top: 20px;">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-success btn-lg btn-block">
                                                    <i class="fa fa-check"></i>
                                                    Finish
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <hr>
                                <h4 class="small" style="margin-top: 20px;">
                                    <i class="fa fa-warning"></i>&nbsp;Invalid URL?
                                </h4>
                                <p class="small">
                                    Your Amazon profile must be set up in order
                                    for us to verify your account.
                                    <a href="https://www.amazon.com/gp/profile" target="_blank">Please click
                                        here</a> and, if prompted, press "save." Then
                                    follow the steps listed above and try again.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php echo form_close() ?>
    </div>