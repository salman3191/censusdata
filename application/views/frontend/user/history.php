<div class="container marketing">
    <!-- Three columns of text below the carousel -->
    <div class="row">
        <div class="col-md-3" style="text-align: center; border-right:1px solid #ccc;">
            <div><img src="<?php echo $profile['img_src']; ?>"></div>
            <div><h3><?php echo $profile['user_name']; ?></h3></div>
            <div><p><?php echo $profile['email']; ?></p></div>
            <div style="margin-top: 15px"><a href="<?php echo base_url() . "users/edit_profile" ?>"
                                             class="btn-block btn btn-default">Edit Profile</a></div>
            <div style="margin-top: 15px"><a href="<?php echo base_url() . "users/snag_history" ?>"
                                             class="btn-block btn btn-default">Snags History</a></div>
            <div style="margin-top: 15px"><a href="<?php echo base_url() . "users/edit_password" ?>"
                                             class="btn-block btn btn-default">Change Password</a></div>
        </div>
        <div class="col-md-9">
            <?php foreach ($snaghistory as $marketing) { ?>
                <div calss="col-ms-12"
                     style="margin-bottom: 10px; padding:10px;border: 1px solid #cccccc; height:200px;">
                    <div class="col-md-4" style="width:275px; height:180px; text-align: center;  border-right: 1px solid #cccccc;">
                        <img style="width: 100%; height: 100%; object-fit: contain;" src="<?php echo $marketing['img_src']; ?>">
                    </div>
                    <div class="col-md-8">
                        <div class="col-md-12" style="margin-top: 20px;">
                            <div class="col-md-4"><span
                                    style="font-size: 14px; font-weight: 200;"> Product Name : </span>
                            </div>
                            <div class="col-md-8"><span
                                    style="font-size: 14px; "><?php echo $marketing['product_name']; ?></span>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 20px; border-top: 1px solid #ccc;">
                            <div class="col-md-4" style="margin-top: 10px;"><span
                                    style="font-size: 14px; font-weight: 200;"> Coupon Code : </span>
                            </div>
                            <div class="col-md-8" style="margin-top: 10px;"><span
                                    style="font-size: 14px; "><?php echo $marketing['coupon_code']; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div><!-- /.row -->