<div class="container" >
    <div class="row">
        <div class="col-lg-12">
            <div class="col-md-offset-3 col-md-6" style="margin-top: 20%;">
                <label class="col-lg-12" style="text-align: center;">Sign in to start your session</label>
                <?php echo ($message) ? $message : ''; ?>
                <form method="POST" action="<?php echo site_url("login") ?>" id="registrationForm"><hr>
                    <div class="input-group">
                        <span class="input-group-addon white-form-bg"><span class="glyphicon glyphicon-user"></span></span>
                        <input id="email-in" name="txtEmail" class="form-control" required placeholder="Email" type="text">
                    </div>
                    <br/>
                    <div class="input-group">
                        <span class="input-group-addon white-form-bg"><span class="glyphicon glyphicon-lock"></span></span>
                        <input id="password" name="txtPassword" class="form-control" placeholder="Password" type="password">
                    </div>

                    <!--                <div class="form-group">
                                        <label for="password" class="col-md-3 label-heading asterisk">Remember Me?</label>
                                        <div class="col-md-9">
                                            <input type="checkbox" value="" name="chkRememberMe" class="form-control" id="chkRememberMe" />
                                        </div>
                                    </div>-->
                    <div class="col-md-offset-3 col-md-6" style="margin-top: 10px;">
                        <input type="submit" name="submit" class="btn btn-primary form-control" value="LogIn" />
                    </div>  
                </form>
            </div>
        </div>
    </div>
</div>