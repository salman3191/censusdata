$.noConflict();
$(document).ready(function () {
    jQuery("#add-user-form").validate({
        rules: {
            first_name: "required",
            last_name: "required",
            username: {
                required: true,
                minlength: 2
            },
            password: {
                required: true,
                minlength: 5
            },
            password2: {
                required: true,
                minlength: 5,
                equalTo: "#password-in"
            },
            email: {
                required: true,
                email: true
            },
        },
        messages: {
            first_name: "Please enter your First name",
            last_name: "Please enter your Last name",
            username: {
                required: "Please enter a User name",
                minlength: "Your username must consist of at least 2 characters"
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            confirm_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            },
            email: "Please enter a valid email address",
        }
    });
});