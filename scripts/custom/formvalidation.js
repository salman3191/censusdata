$.noConflict();
$(document).ready(function () {
    jQuery("#add_account_form").validate({
        rules: {
            first_name: "required",
            last_name: "required",
            company: "required",
            username: {
                required: true,
                minlength: 2
            },
            password: {
                required: true,
                minlength: 5
            },
            password2: {
                required: true,
                minlength: 5,
                equalTo: "#password-in"
            },
            email: {
                required: true,
                email: true
            },
        },
        messages: {
            first_name: "Please enter your First name",
            last_name: "Please enter your Last name",
            company: "Please enter your company name",
            username: {
                required: "Please enter a User name",
                minlength: "Your username must consist of at least 2 characters"
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            confirm_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            },
            email: "Please enter a valid email address",
        }
    });

    // Account Edit Form Validation
    jQuery("#edit-account-form").validate({
        rules: {
            company: "required",
        },
        messages: {
            company: "Please enter your company name",
        }
    });

    // Developer Account Add Form Validation
    jQuery("#dev_add_account_form").validate({
        rules: {
            marketplace: "required",
            access_key: "required",
            secret_key: "required",
        },
        messages: {
            marketplace: "Please select Marketplace",
            access_key: "Please enter MWS Access Key",
            secret_key: "Please enter MWS Secret Key",
        }
    });

    // Developer Account Edit Form Validation
    jQuery("#edit_dev_account_form").validate({
        rules: {
            selectcheck: "required",
            access_key: "required",
            secret_key: "required",
        },
        messages: {
            marketplace: "Please select Marketplace",
            access_key: "Please enter MWS Access Key",
            secret_key: "Please enter MWS Secret Key",
        }
    });

    // Marketplace Add Form Validation
    jQuery("#add_marketplace_account_form").validate({
        rules: {
            marketplace_id: "required",
            marketplace_name: "required",
            host: "required",
        },
        messages: {
            marketplace_id: "Please enter MWS Marketplace ID",
            marketplace_name: "Please enter MWS Marketplace Name",
            host: "Please enter MWS Marketplace Host",
        }
    });

    // Marketplace Edit Form Validation
    jQuery("#edit_marketplace_account").validate({
        rules: {
            marketplace_id: "required",
            marketplace_name: "required",
            host: "required",
        },
        messages: {
            marketplace_id: "Please enter MWS Marketplace ID",
            marketplace_name: "Please enter MWS Marketplace Name",
            host: "Please enter MWS Marketplace Host",
        }
    });

    // User Edit Form Validation
    jQuery("#edit_user_setting").validate({
        rules: {
            first_name: "required",
            last_name: "required",
        },
        messages: {
            first_name: "Please enter your First name",
            last_name: "Please enter your Last name",
        }
    });

    //feedback_setting form validation

    jQuery("#feedback_setting_form").validate({
        rules: {
            amazonstorename: {
                required: true,
            },
            feedbackfromemailaddress: {
                required: true,
                email: true
            },
            mws_sellerid: {
                required: true,
            },
            mws_marketplaceid: "required",
            mws_authtoken: "required"
        },
        messages: {
            amazonstorename: {
                required: "Please enter a Amazone Store Name",
            },
            feedbackfromemailaddress: "Please enter a valid email address",
            mws_sellerid: "Please enter a Amazone Seller Id",
            mws_marketplaceid: "Please select a Amazone Marketplace Id"
        }
    });

    //feedback_alert_setting form validation

    jQuery("#feedback_alert_from").validate({
        rules: {
            set_notification_fbalerts_email: {
                required: true,
                email: true
            },
        },
        messages: {
            set_notification_fbalerts_email: "Please enter a valid email address",
        }
    });

    //Product campaign form validation
    jQuery("#add_campaign_form").validate({
        rules: {
            campaign_name: {
                required: true,
            },
            start_date: {
                required: true,
            },
            end_date: {
                required: true,
            },
        },
        messages: {
            campaign_name: "Please enter a Campaign Name",
            start_date: "Please select a Start Date",
            end_date: "Please select a End Date",
        }
    });

    //Product form validation
    jQuery("#add_product_form").validate({
        rules: {
            product_asin: {
                required: true,
            },
            product_name: {
                required: true,
            },
            retail_price: {
                required: true,
            },
            categories: {
                required: true,
            },
            main_category: {
                required: true,
            },
            merchant_id: {
                required: true,
            },
        },
        messages: {
            product_asin: "Product ASIN is required",
            product_name: "Product ASIN is required",
            retail_price: "Product Retail Price is required",
            categories: "Please select atleast one product category",
            main_category: "Please select atleast one main product category",
            merchant_id: "Merchant ID is required",
        }
    });

    //promotion form validation
    jQuery("#add_promotion_form").validate({
        rules: {
            promo_price: {
                required: true,
            },
            code_type: {
                required: true,
            },
            file_promo_code: {
                required: true,
            },
            categories: {
                required: true,
            },
            main_category: {
                required: true,
            },
            merchant_id: {
                required: true,
            },
        },
        messages: {
            product_asin: "Product ASIN is required",
            product_name: "Product ASIN is required",
            retail_price: "Product Retail Price is required",
            categories: "Please select atleast one product category",
            main_category: "Please select atleast one main product category",
            merchant_id: "Merchant ID is required",
        }
    });

    //Product campaign form validation
    jQuery("#update_campaign_form").validate({
        rules: {
            campaign_name: {
                required: true,
            },
            start_date: {
                required: true,
            },
            end_date: {
                required: true,
            },
        },
        messages: {
            campaign_name: "Please enter a Campaign Name",
            start_date: "Please select a Start Date",
            end_date: "Please select a End Date",
        }
    });

    //Product form validation
    jQuery("#update_product_form").validate({
        rules: {
            product_asin: {
                required: true,
            },
            product_name: {
                required: true,
            },
            retail_price: {
                required: true,
            },
            categories: {
                required: true,
            },
            main_category: {
                required: true,
            },
            merchant_id: {
                required: true,
            },
        },
        messages: {
            product_asin: "Product ASIN is required",
            product_name: "Product ASIN is required",
            retail_price: "Product Retail Price is required",
            categories: "Please select atleast one product category",
            main_category: "Please select atleast one main product category",
            merchant_id: "Merchant ID is required",
        }
    });

    //promotion form validation
    jQuery("#update_promotion_form").validate({
        rules: {
            promo_price: {
                required: true,
            },
            code_type: {
                required: true,
            },
            file_promo_code: {
                required: true,
            },
            categories: {
                required: true,
            },
            main_category: {
                required: true,
            },
            merchant_id: {
                required: true,
            },
        },
        messages: {
            product_asin: "Product ASIN is required",
            product_name: "Product ASIN is required",
            retail_price: "Product Retail Price is required",
            categories: "Please select atleast one product category",
            main_category: "Please select atleast one main product category",
            merchant_id: "Merchant ID is required",
        }
    });

});